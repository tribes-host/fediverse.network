defmodule Fd.ActivityScore do
  use GenServer
  require Logger

  @interval :timer.minutes(10)

  def start_link() do
    GenServer.start_link(__MODULE__, [], [name: __MODULE__])
  end

  def rebuild() do
    GenServer.call(__MODULE__, :build)
  end

  def get() do
    :persistent_term.get(__MODULE__)
  end

  def init(_) do
    Logger.debug "ActivityScore: building"
    :persistent_term.put(__MODULE__, %{})
    build()
    timer = :erlang.send_after(@interval, self(), :build)
    {:ok, %{timer: timer}}
  end

  def handle_call(:build, _from, state) do
    :erlang.cancel_timer(Map.get(state, :timer))
    timer = :erlang.send_after(@interval, self(), :build)
    {:reply, build(), %{timer: timer}}
  end

  def handle_info(:build, state) do
    Logger.debug "ActivityScore: rebuilding"
    :ok = build()
    timer = :erlang.send_after(@interval, self(), :build)
    {:noreply, %{timer: timer}}
  end

  def build do
    periods = %{
      "1h" => {-1, "hour"},
      "3h" => {-3, "hour"},
      "6h" => {-6, "hour"},
      "12h" => {-12, "hour"},
      "24h" => {-24, "hour"},
      "7d" => {-7, "day"}
    }

    counts = Enum.reduce(periods, Map.new, fn({period_name, period}, acc) ->
      Map.put(acc, period_name, build_period(period))
    end)
    :persistent_term.put(__MODULE__, counts)
    :ok
  end

  def build_period(_period = {count,interval}) do
    import Ecto.Query
    alias Fd.{Repo, Instances.Instance, Instances.InstanceCheck}

    counts = from(i in InstanceCheck,
      select: %{instance_id: i.instance_id,
        new_statuses: fragment("last(?,?) - first(?,?) as statuses_diff", i.statuses, i.updated_at, i.statuses, i.updated_at),
        new_users: fragment("last(?,?) - first(?,?) as users_diff", i.users, i.updated_at, i.users, i.updated_at),
      },
      where: fragment("? between (?) AND (now() at time zone 'utc')", i.updated_at, from_now(^count, ^interval)),
      group_by: [i.instance_id]
    )
    |> Repo.all(timeout: :timer.minutes(2))
    |> Enum.reduce(Map.new, fn(result, acc) ->
      unless Map.get(acc, result.instance_id) do

        entry = result
        |> Map.drop([:instance_id])
        |> Enum.map(fn({key, value}) ->
          value = cond do
            value >= 0 -> value
            is_integer(value) or is_float(value) -> 0.0
            true -> value
          end
          {key, value}
        end)
        |> Enum.into(Map.new)

          Map.put(acc, result.instance_id, entry)
      else
        acc
      end
    end)

    {total_status, total_users} = Enum.reduce(counts, {0, 0}, fn({id, %{new_statuses: ns, new_users: nu}}, {status, users}) ->
      {status+(ns||0), users+(nu||0)}
    end)

    counts = Enum.reduce(counts, Map.new, fn({id, entry}, acc) ->
      status_score = if entry.new_statuses <= 0 or !entry.new_statuses do
        0
      else
        ((entry.new_statuses / total_status*1.0))
      end
      users_score = if entry.new_users <= 0 or !entry.new_users do
        0
      else
        ((entry.new_users / total_users*1.0))
      end

      users_weight = 0.15
      statuses_weight = 0.85
      score = ( (users_score*users_weight) + (status_score*statuses_weight) )

      entry = entry
      |> Map.put(:activity_score, score)
      |> Map.put(:status_activity_fraction, status_score)
      |> Map.put(:user_activity_fraction, users_score)
      Map.put(acc, id, entry)
    end)

    counts = counts
    |> Enum.sort_by(fn({_, c}) -> c.status_activity_fraction end, &>=/2)
    |> Enum.with_index()
    |> Enum.reduce(Map.new, fn({{key, result}, idx}, acc) ->
      data = result
      |> Map.put(:status_rank, idx+1)
      Map.put(acc, key, data)
    end)

    counts = counts
    |> Enum.sort_by(fn({_, c}) -> c.user_activity_fraction end, &>=/2)
    |> Enum.with_index()
    |> Enum.reduce(Map.new, fn({{key, result}, idx}, acc) ->
      data = result
      |> Map.put(:user_rank, idx+1)
      Map.put(acc, key, data)
    end)

    counts = counts
    |> Enum.sort_by(fn({_, c}) -> c.activity_score end, &>=/2)
    |> Enum.with_index()
    |> Enum.reduce(Map.new, fn({{key, result}, idx}, acc) ->
      data = result
      |> Map.put(:activity_rank, idx+1)
      Map.put(acc, key, data)
    end)


    counts = counts
    |> Map.put(:new_statuses, total_status)
    |> Map.put(:new_users, total_users)

  end


  def positive_number(int) when is_integer(int) or is_float(int) and int > 0 do
    int
  end

  def positive_number(_), do: 0.0

end

