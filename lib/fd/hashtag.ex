defmodule Fd.Hashtag do
  use Ecto.Schema
  import Ecto.Changeset
  alias __MODULE__

  @primary_key false

  schema "hashtags" do
    field :ihash, :integer
    field :tag, :string
    field :lang, :string
    field :created_at, :naive_datetime
    belongs_to :instance, Fd.Instances.Instance
  end

  def changeset(%__MODULE__{} = schema, attrs) do
    schema
    |> cast(attrs, [:hash, :tag, :lang, :instance_id, :created_at])
  end

end
