defmodule Fd.Isp do

  @isps %{
    "Write.as" => %{
      link: "https://write.as"
    },
    "Write.as for Teams" => %{
      link: "https://write.as/for/teams"
    },
    "WriteFreely.host" => %{
      link: "https://writefreely.host"
    },
    "masto.host" => %{
      link: "https://masto.host"
    }
  }

  for {isp, data} <- @isps do
    def data(unquote(isp)), do: unquote(Macro.escape(data))
  end

  def data(_), do: %{}
end
