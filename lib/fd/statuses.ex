defmodule Fd.Status do
  use Ecto.Schema
  import Ecto.Changeset
  alias __MODULE__

  @primary_key false

  schema "statuses" do
    field :ihash, :integer
    field :uhash, :integer
    field :lang, :string
    field :at, :naive_datetime
    field :updated_at, :naive_datetime
    belongs_to :instance, Fd.Instances.Instance
  end

  def changeset(%__MODULE__{} = schema, attrs) do
    schema
    |> cast(attrs, [:ihash, :uhash, :lang, :at, :instance_id, :updated_at])
  end

end
