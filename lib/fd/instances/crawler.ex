defmodule Fd.Instances.Crawler do
  @moduledoc """
  # Instance Crawler

  1. Test Masto API (/api/v1/instance)
      1. a. /api/v1/instance/peers (only once per 6h)
      1. b. /api/v1/instance/custom_emojis (only once per 24h)
  2. Test Statusnet API (/api/statusnet/config)
      2. a. /api/statusnet/config
      2. b. Test /main/statistics
  3. Update `Instance`

  Maybe:
    - Could test if we get a reply on /api/z/1.0/channel/stream (so we can know it's hubzilla?)

  """

  # TODO: Store complete crawl state into Instance db

  require Logger
  alias __MODULE__
  alias Fd.{Instances}
  alias Fd.Instances.{Instance, InstanceCheck, Instrumenter}

  defmacro __using__(_) do
    quote do
      alias Fd.Repo
      alias Fd.Instances
      alias Fd.Instances.{Crawler, Instance}
      require Logger
      import Crawler, only: [debug: 2, info: 2, error: 2, request: 2, request: 3], warn: false
    end
  end

  @hackney_pool :hackney_crawler
  @hackney_pool_opts [{:timeout, 50_000}, {:max_connections, 200}, {:connect_timeout, 50_000}]
  @hackney_mon_pool :hackney_crawler_mon
  @hackney_mon_pool_opts [{:timeout, 50_000}, {:max_connections, 100}, {:connect_timeout, 50_000}]
  @hackney_dead_pool :hackney_crawler_dead
  @hackney_dead_pool_opts [{:timeout, 50_000}, {:max_connections, 100}, {:connect_timeout, 150_000}]
  @hackney_opts [{:connect_timeout, 150_000}, {:recv_timeout, 50_000}, {:pool, @hackney_pool}]
  @hackney_mon_opts [{:pool, @hackney_mon_pool}]
  @hackney_dead_opts [{:pool, @hackney_dead_pool}]

  @down_http_codes [301, 410, 502, 503, 504, 505, 520, 521, 522, 523, 524, 525, 526, 527, 530]
  @retry_http_codes [500, 502, 503, 504, 505, 520, 521, 522, 523, 524]
  @nodeinfo_servers ["hubzilla", "Friendica"]
  @nodeinfo_hide_if_not_found_servers ["Friendica"]

  def down_http_codes, do: @down_http_codes
  def nodeinfo_servers, do: @nodeinfo_servers
  def nodeinfo_hide_if_not_found_servers, do: @nodeinfo_hide_if_not_found_servers

  @callback query(t()) :: t()
  @callback process_results(t()) :: t()
  @callback after_run(t()) :: t()

  @pipelines %{
    {"process_results", "final"} => [Crawler.Server, Crawler.Semver, Crawler.PublicSuffix, Crawler.HostInfo,
      Crawler.Changes],
    "after_run" => [Crawler.Server, Crawler.Peers, Crawler.Fedibot, Crawler.MonitoringAlert]
  }

  defstruct [ :instance,
              :halted?,
              :fatal_error,
              :server,

              :has_mastapi?,
              :m_instance,
              :m_peers,
              :m_custom_emojis,

              :has_statusnet?,
              :s_version,
              :s_config,

              :has_peertubeapi?,
              :pt_config,
              :pt_stats,

              :has_nodeinfo?,
              :nodeinfo_schema,
              :nodeinfo,

              :has_misskey?,
              :misskey_meta,
              :misskey_stats,

              :html,

              :changes,
              :check,
              :diffs,
            ]

  @type t :: %__MODULE__{}

  def setup() do
    :ok = :hackney_pool.start_pool(@hackney_pool, @hackney_pool_opts)
    :ok = :hackney_pool.start_pool(@hackney_mon_pool, @hackney_mon_pool_opts)
    :ok = :hackney_pool.start_pool(@hackney_dead_pool, @hackney_dead_pool_opts)
  end

  def put_new(map, key, value) do
    unless Map.get(map, key) do
      put(map, key, value)
    else
      map
    end
  end
  def put(map, key, value) when value in ["", nil] do
    map
  end
  def put(map, key, value) do
    Map.put(map, key, value)
  end

  def run(instance) do
    state = %Crawler{instance: instance, halted?: false, has_mastapi?: false, has_statusnet?: false, has_peertubeapi?:
      false, has_nodeinfo?: false, has_misskey?: false, changes: %{}, check: %{}, diffs: %{}}

    start = :erlang.monotonic_time
    info(state, "starting new crawl")

    # Main pipeline
    state = run_pipeline(state)
    # Final process_results
    mods = Map.get(@pipelines, {"process_results", "final"}, [])
    state = apply_safe_callbacks(state, mods, :process_results)

    pipeline_stop = :erlang.monotonic_time

    changes = Map.get(state, :changes, %{})
              |> Map.put("last_checked_at", DateTime.utc_now())
    state = %Crawler{state | changes: changes}

    debug(state, "changes: #{inspect changes}")

    check_changeset = InstanceCheck.changeset(%InstanceCheck{instance_id: instance.id}, state.check)
    check = Fd.Repo.insert!(check_changeset)
    state = %Crawler{state | check: check}
    state = case Instances.update_instance(instance, state.changes) do
      {:ok, instance} ->

        state = %Crawler{state | instance: instance}

        # After run pipeline
        mods = Map.get(@pipelines, "after_run", [])
        state = apply_safe_callbacks(state, mods, :after_run)

        warn(state, "Checked! #{inspect state.diffs}")
        state
      error ->
        error(state, "FAIL: #{inspect error}")
        state
    end

    finished = :erlang.monotonic_time
    pipeline_duration = pipeline_stop - start
    total_duration = finished - start

    warn(state, "finished in #{:erlang.convert_time_unit(total_duration, :native, :millisecond)}ms (pipeline took #{:erlang.convert_time_unit(pipeline_duration, :native, :millisecond)} ms)!")

    state
  end

  def run_pipeline(state = %{instance: %Instance{server: 2, domain: domain}}) do
    state
    |> query_mastapi_instance()
    |> query_mastapi_peers()
    |> query_mastapi_emojis()
    |> Crawler.Nodeinfo.query()
    |> process_results()
    |> Crawler.Nodeinfo.process_results()
  end

  def run_pipeline(state = %{instance: %Instance{server: 3, domain: domain}}) do
    state
    |> query_mastapi_instance()
    |> query_mastapi_peers()
    |> query_mastapi_emojis()
    |> query_statusnet_config()
    |> Crawler.Nodeinfo.query()
    |> process_results()
    |> Crawler.Nodeinfo.process_results()
  end

  @gnusoc_servers [1, 5, 6]
  def run_pipeline(state = %{instance: %Instance{server: srv, domain: domain}}) when srv in @gnusoc_servers do
    state
    |> query_statusnet_version()
    |> query_statusnet_config()
    |> query_statusnet_config2()
    |> Crawler.Nodeinfo.query()
    |> process_results()
    |> Crawler.Nodeinfo.process_results()
  end

  @nodeinfo_servers [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
    32, 33, 34, 35, 36, 37, 38, 39]
  def run_pipeline(state = %{instance: %Instance{server: srv, domain: domain}}) when srv in @nodeinfo_servers do
    state
    |> Crawler.Nodeinfo.query()
    |> process_results()
    |> Crawler.Nodeinfo.process_results()
  end

  def run_pipeline(state) do
    state
    |> query_mastapi_instance()
    |> query_mastapi_peers()
    |> query_mastapi_emojis()
    |> query_statusnet_version()
    |> query_statusnet_config()
    |> query_peertube_config()
    |> query_peertube_stats()
    |> query_statusnet_config2()
    |> Crawler.Nodeinfo.query()
    |> query_misskey_meta()
    |> query_misskey_stats()
    #|> query_html_index()
    |> process_results()
    |> Crawler.Nodeinfo.process_results()
  end


  def process_results(crawler = %Crawler{halted?: true}) do
    error_s = Fd.HumanError.format(crawler.fatal_error)
    check = %{"up" => false, "error_s" => error_s}

    changes = crawler.changes
    |> Map.put("last_down_at", DateTime.utc_now())
    |> Map.put("up", false)
    %Crawler{crawler | changes: changes, check: check}
  end

  def process_results(crawler = %Crawler{has_mastapi?: true}) do
    changes = %{"last_up_at" => DateTime.utc_now(), "has_mastapi" => true}

    stats = Map.get(crawler.m_instance, "stats", %{})
    {server, version} = Map.get(crawler.m_instance, "version", nil) |> process_mastapi_version()
    user_count = Map.get(stats, "user_count")
    peer_count = Map.get(stats, "domain_count")
    status_count = Map.get(stats, "status_count")
    name = Map.get(crawler.m_instance, "title", crawler.instance.domain)
    description = Map.get(crawler.m_instance, "description")
    email = Map.get(crawler.m_instance, "email")
    emojis = Enum.reduce(crawler.m_custom_emojis || [], %{}, fn
      (%{"url" => url, "shortcode" => code}, emojis) -> Map.put(emojis, code, url)
      (_, emojis) -> emojis
    end)
    emoji_count = Enum.count(emojis)
    max_chars = Map.get(crawler.m_instance, "max_toot_chars", 500)
    contact_account = Map.get(crawler.m_instance, "contact_account", %{})
    registrations = Map.get(crawler.m_instance, "registrations")

    signup = cond do
      registrations == true -> true
      registrations == false -> false
      # FIXME: this was for pleroma but not needed anymore
      crawler.has_statusnet? ->
        if get_in(crawler.s_config, ["site", "closed"]) do
          get_in(crawler.s_config, ["site", "closed"]) == "0"
        else
          nil
        end
      true -> nil
    end

    # gab check
    is_gab = cond do
      name == "Gab Social" -> true
      Enum.member?(Map.keys(contact_account || %{}), "is_pro") && Enum.member?(Map.keys(contact_account || %{}), "is_verified") -> true
      true -> false
    end

    version = if is_gab, do: "#{version}+gab", else: version

    check = %{"up" => true, "users" => user_count, "peers" => peer_count, "statuses" => status_count, "emojis" =>
      emoji_count, "version" => version, "signup" => signup, "max_chars" => max_chars, "server" => Fd.ServerName.to_int(server)}

    changes = changes
    |> Map.put("custom_emojis", emojis)
    |> Map.put("mastapi_instance", crawler.m_instance)
    |> Map.put("statusnet_config", crawler.s_config)
    |> Map.put("server", Fd.ServerName.to_int(server))
    |> Map.put("name", name)
    |> Map.put("description", description)
    |> Map.put("email", email)
    |> Map.put("dead", false)
    |> Map.merge(check)

    %Crawler{crawler | changes: changes, check: check}
  end

  def process_results(crawler = %Crawler{has_peertubeapi?: true}) do
    changes = %{"last_up_at" => DateTime.utc_now()}

    version = Map.get(crawler.pt_config, "serverVersion")
    server = "peertube"
    signup = get_in(crawler.pt_config, ["signup", "allowed"])
    stats = crawler.pt_stats || %{}
    videos = Map.get(stats, "totalLocalVideos", 0)
    comments = Map.get(stats, "totalLocalVideoComments", 0)
    statuses = videos + comments
    users = Map.get(stats, "totalUsers")

    check = %{"up" => true, "version" => version, "signup" => signup, "users" => users, "statuses" => statuses, "server" => Fd.ServerName.to_int(server)}

    changes = changes
    |> Map.put("peertube_config", crawler.pt_config)
    |> Map.put("server", Fd.ServerName.to_int(server))
    |> Map.put("dead", false)
    |> Map.merge(check)

    %Crawler{crawler | changes: changes, check: check}
  end

  @falses ["0", "false", false]
  @trues ["1", "true", true]

  # Hubzilla reports there
  def process_results(crawler = %Crawler{has_statusnet?: true}) do
    changes = %{"last_up_at" => DateTime.utc_now()}
    invite_only = cond do
      get_in(crawler.s_config, ["site", "inviteonly"]) in @trues -> true
      true -> false
    end
    signup = cond do
      invite_only -> false
      get_in(crawler.s_config, ["site", "closed"]) in @falses -> true
      get_in(crawler.s_config, ["site", "closed"]) in @trues -> false
      true -> nil
    end
    max_chars = get_in(crawler.s_config, ["site", "textlimit"])
    name = get_in(crawler.s_config, ["site", "name"])
    email = get_in(crawler.s_config, ["site", "email"])

    server = cond do
      platform = get_in(crawler.s_config, ["site", "platform", "PLATFORM_NAME"]) -> platform
      friendica = get_in(crawler.s_config, ["site", "friendica", "FRIENDICA_PLATFORM"]) -> friendica
      crawler.s_version ->
        {s, _} = process_statusnet_version(crawler.s_version)
        s
      true -> "GNUSocial"
    end
    private = cond do
      server in @nodeinfo_hide_if_not_found_servers and !crawler.has_nodeinfo? -> true
      get_in(crawler.s_config, ["site", "private"]) in @trues -> true
      true -> false
    end

    version = cond do
      platform = get_in(crawler.s_config, ["site", "platform", "STD_VERSION"]) -> platform
      friendica = get_in(crawler.s_config, ["site", "friendica", "FRIENDICA_VERSION"]) -> friendica
      crawler.s_version ->
        {_, v} = process_statusnet_version(crawler.s_version)
        v
      true -> nil
    end

    check = %{"up" => true, "signup" => signup, "max_chars" => max_chars, "version" => version, "server" => Fd.ServerName.to_int(server)}

    # Nodeinfo data is returned by Hubzilla (and coming from Disapora). For now we only use/crawl it if we already know
    # it's a hubzilla server we are talking to (using statusnet api); see query_nodeinfo/1
    check = if crawler.has_nodeinfo? do
      users = get_in(crawler.nodeinfo, ["usage", "users", "total"])
      posts = get_in(crawler.nodeinfo, ["usage", "localPosts"])
      comments = get_in(crawler.nodeinfo, ["usage", "localComments"])
      statuses = posts + comments
      check
      |> Map.put("users", users)
      |> Map.put("statuses", statuses)
    else check end

    changes = changes
    |> Map.put("statusnet_config", crawler.s_config)
    |> Map.put("server", Fd.ServerName.to_int(server))
    |> Map.put("version", version)
    |> Map.put("email", email)
    |> Map.put("name", name)
    |> Map.put("dead", false)
    |> Map.put("hidden", private)
    |> Map.merge(check)

    %Crawler{crawler | changes: changes, check: check}
  end

  def process_results(crawler = %{has_misskey?: true}) do
    users = Map.get(crawler.misskey_stats || %{}, "originalUsersCount")
    statuses = Map.get(crawler.misskey_stats || %{}, "originalNotesCount")
    version = Map.get(crawler.misskey_meta || %{}, "version")
    server = Fd.ServerName.to_int("misskey")

    check = %{"up" => true, "users" => users, "statuses" => statuses, "version" => version, "server" => server}
    changes = %{"last_up_at" => DateTime.utc_now()}
    |> Map.merge(check)
    |> Map.put("dead", false)

    %Crawler{crawler | changes: changes, check: check}
  end

  def process_results(crawler) do
    Logger.warn "Unprocessable results for #{crawler.instance.domain} (id #{crawler.instance.id}) -- #{inspect crawler}"
  #  check = %{"up" => true}
  #  changes = crawler.changes || %{}
  #  |> Map.put("last_up_at", DateTime.utc_now())
  #  |> Map.put("server", 0)
  #  |> Map.put("up", true)
  #  %Crawler{crawler | changes: changes, check: check}
    crawler
  end


  defp process_statusnet_version("postactiv-"<>version), do: {"PostActiv", version}
  defp process_statusnet_version("Pleroma "<>version), do: {"Pleroma", version}
  defp process_statusnet_version(version), do: {"GNUSocial", version}


  defp process_mastapi_version(string) when is_binary(string) do
    {server, version} = cond do
      # Old misskey
      String.contains?(string, ":compatible:") ->
        [_, server_and_version] = String.split(string, ":compatible:", parts: 2)
        case String.split(server_and_version, [",", " ", ":"], parts: 2) do
          [server, version] -> {server, version}
          _ -> {nil, server_and_version}
        end
      # "universal" compatible (pleroma-like) format: "masto_version; compatible ServerName real_version"
      #                                               "version; compatible ServerName"
      String.contains?(string, "compatible;") ->
        [main_version, server_and_version] = String.split(string, "(compatible; ")
        case String.split(server_and_version, " ", parts: 2) do
          [server] -> {server, main_version}
          [server, version] -> {server, version}
        end
      # Old versions of Pleroma
      String.starts_with?(string, "Pleroma") ->
        [_, version] = String.split(string, " ", parts: 2)
        {"Pleroma", version}
      string == "Mastodon::Version" -> {"Mastodon", "1.3"}
      true ->
        {"Mastodon", string}
    end
    {downcase(clean_string(server)), clean_string(version)}
  end
  defp process_mastapi_version(_), do: {"Unknown", nil}

  defp clean_string(string) do
    string
    |> String.trim_trailing(")")
    |> String.trim_trailing(".")
    |> String.trim_trailing()
  end

  #
  # MastAPI: Instance
  #

  @mastapi_not_found [404]
  def query_mastapi_instance(crawler) do
    case request(crawler, "/api/v1/instance") do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        debug(crawler, "got /api/v1/instance " <> inspect(body))
        %Crawler{crawler | has_mastapi?: true, m_instance: body}
      {:ok, %HTTPoison.Response{status_code: code}} when code in @mastapi_not_found ->
        debug(crawler, "mastapi is not found. #{inspect code}")
        %Crawler{crawler | has_mastapi?: false}
      {:ok, %HTTPoison.Response{status_code: code}} when code not in @down_http_codes ->
        debug(crawler, "mastapi responded with an invalid code, maybe down or not found: #{inspect code}")
        %Crawler{crawler | has_mastapi?: false}
      {:error, %Jason.DecodeError{}} ->
        debug(crawler, "json decode error, skipping")
        %Crawler{crawler | has_mastapi?: false}
      failed ->
        debug(crawler, "host is down " <> inspect(failed))
        %Crawler{crawler | halted?: true, fatal_error: failed}
    end
  end

  #
  # MastAPI: Peers
  #

  def query_mastapi_peers(crawler = %Crawler{has_mastapi?: true, halted?: false}) do
    case request(crawler, "/api/v1/instance/peers") do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        debug(crawler, "got /api/v1/instance/peers " <> inspect(body))
        %Crawler{crawler | has_mastapi?: true, m_peers: body}
      {:ok, %HTTPoison.Response{status_code: code}} when code not in @down_http_codes ->
        debug(crawler, "mastapi responded with an invalid code, maybe down or not found: #{inspect code}")
        crawler
      {:error, %Jason.DecodeError{}} ->
        debug(crawler, "json decode error, skipping")
        crawler
      failed ->
        debug(crawler, "host is down " <> inspect(failed))
        %Crawler{crawler | halted?: true, fatal_error: failed}
    end
  end

  def query_mastapi_peers(crawler), do: crawler

  #
  # MastAPI: Emojis
  #

  def query_mastapi_emojis(crawler = %Crawler{has_mastapi?: true, halted?: false}) do
    case request(crawler, "/api/v1/custom_emojis") do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        debug(crawler, "got /api/v1/custom_emojis " <> inspect(body))
        %Crawler{crawler | has_mastapi?: true, m_custom_emojis: body}
      {:ok, %HTTPoison.Response{status_code: code}} when code not in @down_http_codes ->
        debug(crawler, "mastapi responded with an invalid code, maybe down or not found: #{inspect code}")
        crawler
      {:error, %Jason.DecodeError{}} ->
        debug(crawler, "json decode error, skipping")
        crawler
      failed ->
        debug(crawler, "host is down " <> inspect(failed))
        %Crawler{crawler | halted?: true, fatal_error: failed}
    end
  end

  def query_mastapi_emojis(crawler), do: crawler

  #
  # PeerTubeAPI: Config
  #

  @mastapi_not_found [404]
  def query_peertube_config(crawler = %Crawler{halted?: false, has_mastapi?: mastapi, has_statusnet?: false}) when not mastapi do
    case request(crawler, "/api/v1/config") do
      {:ok, %HTTPoison.Response{status_code: 200, body: body = %{"instance" => _, "serverVersion" => _}}} ->
        debug(crawler, "got peertube-like /api/v1/config " <> inspect(body))
        %Crawler{crawler | has_peertubeapi?: true, pt_config: body}
      {:ok, %HTTPoison.Response{status_code: code}} when code in @mastapi_not_found ->
        debug(crawler, "peertubeapi is not found. #{inspect code}")
        %Crawler{crawler | has_peertubeapi?: false}
      {:ok, %HTTPoison.Response{status_code: code}}  when code not in @down_http_codes ->
        debug(crawler, "peertubeapi responded with an invalid code, maybe down or not found: #{inspect code}")
        %Crawler{crawler | has_peertubeapi?: false}
      {:error, %Jason.DecodeError{}} ->
        debug(crawler, "json decode error, skipping")
        %Crawler{crawler | has_peertubeapi?: false}
      failed ->
        debug(crawler, "host is down " <> inspect(failed))
        %Crawler{crawler | halted?: true, fatal_error: failed}
    end
  end

  def query_peertube_config(crawler) do
    debug(crawler, "skipping peertube test")
    crawler
  end
  #
  # PeerTubeAPI: Config
  #

  def query_peertube_stats(crawler = %Crawler{halted?: false, has_peertubeapi?: true}) do
    case request(crawler, "/api/v1/server/stats") do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        debug(crawler, "got peertube-like /api/v1/server/stats " <> inspect(body))
        %Crawler{crawler | has_peertubeapi?: true, pt_stats: body}
      {:ok, %HTTPoison.Response{status_code: code}} when code not in @down_http_codes ->
        debug(crawler, "peertubeapi (stats) responded with an invalid code, maybe down or not found: #{inspect code}")
       crawler
      {:error, %Jason.DecodeError{}} ->
        debug(crawler, "json decode error, skipping")
        crawler
      failed ->
        debug(crawler, "host is down " <> inspect(failed))
        %Crawler{crawler | halted?: true, fatal_error: failed}
    end
  end

  def query_peertube_stats(crawler), do: crawler


  #
  # StatusNet: Version
  #
  @mastapi_not_found [404]
  def query_statusnet_version(crawler = %Crawler{halted?: false, has_mastapi?: false}) do
    case request(crawler, "/api/statusnet/version.json") do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        debug(crawler, "got /api/statusnet/version.json " <> inspect(body))
        %Crawler{crawler | has_statusnet?: true, s_version: body}
      {:ok, %HTTPoison.Response{status_code: code}} when code in @mastapi_not_found ->
        debug(crawler, "statusnet is not found. #{inspect code}")
        %Crawler{crawler | has_statusnet?: false}
      {:ok, %HTTPoison.Response{status_code: code}} when code not in @down_http_codes  ->
        debug(crawler, "statusnet responded with an invalid code, maybe down or not found: #{inspect code}")
        crawler
      {:error, %Jason.DecodeError{}} ->
        debug(crawler, "json decode error, skipping")
        crawler
      failed ->
        debug(crawler, "host is down " <> inspect(failed))
        %Crawler{crawler | halted?: true, fatal_error: failed}
    end
  end

  def query_statusnet_version(crawler), do: crawler

  @mastapi_not_found [404]
  def query_statusnet_config(crawler = %Crawler{halted?: false}) do
    case request(crawler, "/api/statusnet/config.json") do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        debug(crawler, "got /api/statusnet/config.json " <> inspect(body))
        %Crawler{crawler | has_statusnet?: true, s_config: body}
      {:ok, %HTTPoison.Response{status_code: code}} when code in @mastapi_not_found ->
        debug(crawler, "statusnet is not found. #{inspect code}")
        %Crawler{crawler | has_statusnet?: false}
      {:ok, %HTTPoison.Response{status_code: code}} when code not in @down_http_codes  ->
        debug(crawler, "statusnpet responded with an invalid code, maybe down or not found: #{inspect code}")
        crawler
      {:error, %Jason.DecodeError{}} ->
        debug(crawler, "json decode error, skipping")
        crawler
      failed ->
        debug(crawler, "host is down " <> inspect(failed))
        %Crawler{crawler | halted?: true, fatal_error: failed}
    end
  end
  def query_statusnet_config(crawler), do: crawler

  @mastapi_not_found [404]
  def query_statusnet_config2(crawler = %Crawler{halted?: false, has_statusnet?: false, has_mastapi?: false, has_peertubeapi?: false}) do
    case request(crawler, "/api/statusnet/config") do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        debug(crawler, "got /api/statusnet/config " <> inspect(body))
        %Crawler{crawler | has_statusnet?: true, s_config: body}
      {:ok, %HTTPoison.Response{status_code: code}} when code in @mastapi_not_found ->
        debug(crawler, "statusnet is not found. #{inspect code}")
        %Crawler{crawler | has_statusnet?: false}
      {:ok, %HTTPoison.Response{status_code: code}} when code not in @down_http_codes  ->
        debug(crawler, "statusnet responded with an invalid code, maybe down or not found: #{inspect code}")
        crawler
      {:error, %Jason.DecodeError{}} ->
        debug(crawler, "json decode error, skipping")
        crawler
      failed ->
        debug(crawler, "host is down " <> inspect(failed))
        %Crawler{crawler | halted?: true, fatal_error: failed}
    end
  end
  def query_statusnet_config2(crawler), do: crawler

  #
  # -- /nodeinfo/2.0
  #
  # Used by Hubzilla and Disaporas-like
  #
  # Only hit if server has already been discovered and match a pre-defined list.
  #

  @mastapi_not_found [404]

  # -- Misskey /api/meta
  def query_misskey_meta(crawler = %Crawler{halted?: false, has_mastapi?: false, has_statusnet?: false, has_peertubeapi?: false, has_nodeinfo?: false}) do
    case request(crawler, "/api/meta", [method: :post]) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        debug(crawler, "got /api/meta " <> inspect(body))
        %Crawler{crawler | has_misskey?: true, misskey_meta: body}
      {:ok, %HTTPoison.Response{status_code: code}} when code in @mastapi_not_found ->
        debug(crawler, "misskey is not found. #{inspect code}")
        %Crawler{crawler | has_misskey?: false}
      {:ok, %HTTPoison.Response{status_code: code}} when code not in @down_http_codes  ->
        debug(crawler, "misskey responded with an invalid code, maybe down or not found: #{inspect code}")
        crawler
      {:error, %Jason.DecodeError{}} ->
        debug(crawler, "json decode error, skipping")
        %Crawler{crawler | has_misskey?: false}
      failed ->
        debug(crawler, "host is down (meta misskey) " <> inspect(failed))
        %Crawler{crawler | halted?: true, fatal_error: failed}
    end
  end
  def query_misskey_meta(crawler), do: crawler

  def query_misskey_stats(crawler = %Crawler{halted?: false, has_misskey?: true}) do
    case request(crawler, "/api/stats", [method: :post]) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        debug(crawler, "got /api/stats " <> inspect(body))
        %Crawler{crawler | misskey_stats: body}
      {:ok, %HTTPoison.Response{status_code: code}} when code in @mastapi_not_found ->
        debug(crawler, "misskey stats is not found. #{inspect code}")
        crawler
      {:ok, %HTTPoison.Response{status_code: code}} when code not in @down_http_codes  ->
        debug(crawler, "misskey stats responded with an invalid code, maybe down or not found: #{inspect code}")
        crawler
      {:error, %Jason.DecodeError{}} ->
        debug(crawler, "json decode error, skipping")
        crawler
      failed ->
        debug(crawler, "host is down " <> inspect(failed))
        %Crawler{crawler | halted?: true, fatal_error: failed}
    end
  end
  def query_misskey_stats(crawler), do: crawler

  #
  # -- HTML Index
  # -> Detect GNU Social (last resort)
  # -> Detect old Mastodon versions
  # -> Detect Mastodon registration state
  def query_html_index(crawler = %Crawler{halted?: false}) do
    case request(crawler, "/", [json: false, follow_redirects: true]) do
      {:ok, resp = %HTTPoison.Response{status_code: 200, body: body}} ->
        %Crawler{crawler | html: body}
      error ->
        info(crawler, "failed to get index page #{inspect error}")
    end
  end

  def query_html_index(crawler), do: crawler

  def request(crawler, path, options \\ []), do: request(crawler, path, options, 1)

  @env Mix.env
  defp request(crawler = %Crawler{instance: %Instance{domain: domain}}, path, options, retries) do
    follow_redirects = Keyword.get(options, :follow_redirects, false)
    json = Keyword.get(options, :json, true)
    method = Keyword.get(options, :method, :get)
    accept = Keyword.get(options, :accept)
    timeout = Keyword.get(options, :timeout, 15_000)
    recv_timeout = Keyword.get(options, :recv_timeout, 15_000)
    body = Keyword.get(options, :body, "")
    {mon_ua, options} = cond do
      crawler.instance.monitor ->
        mon_ua = " - monitoring enabled https://fediverse.network/monitoring"
        {mon_ua, [hackney: @hackney_mon_opts]}
      crawler.instance.dead || !crawler.instance.last_up_at ->
        {"", [hackney: @hackney_dead_opts]}
      crawler.instance.last_up_at && DateTime.diff(DateTime.utc_now(), crawler.instance.last_up_at) >= 2678400  ->
        {"", [hackney: @hackney_dead_opts]}
      !crawler.instance.server || crawler.instance.server == 0 ->
        {"", [hackney: @hackney_dead_opts]}
      true ->
        {"", [hackney: @hackney_opts]}
    end
    options = [timeout: timeout, recv_timeout: recv_timeout, follow_redirect: follow_redirects] ++ options
    headers = %{
      "User-Agent" => Fd.user_agent("crawler", mon_ua)
    }
    headers = if json do
      Map.put(headers, "Accept", "application/json")
    else headers end
    headers = if accept do
      Map.put(headers, "Accept", accept)
    else headers end
    start = :erlang.monotonic_time
    case HTTPoison.request(method, "https://#{domain}#{path}", body, headers, options) do
      {:ok, response = %HTTPoison.Response{status_code: 200, body: body}} ->
        Instrumenter.http_request(path, response, start)
        info(crawler, "http ok - #{inspect method} - #{inspect path}")
        debug(crawler, "http body: " <>inspect(body))
        case json && Jason.decode(body) do
          {:ok, body} ->
            debug(crawler, "body parsed in json!")
            resp = %HTTPoison.Response{response | body: body}
            {:ok, resp}
          {:ok, other} ->
            info(crawler, "invalid json type, not a map " <> inspect(other))
            {:error, :invalid_json_type}
          {:error, error} ->
            info(crawler, "invalid json: " <> inspect({error, body}))
            {:error, error}
          false -> {:ok, response}
        end
      {:ok, response = %HTTPoison.Response{status_code: code}} when code in @retry_http_codes ->
        Instrumenter.http_request(path, response, start)
        retry(crawler, path, options, {:ok, response}, retries)
      {:ok, response} ->
        Instrumenter.http_request(path, response, start)
        info(crawler, "http ok - #{inspect response.status_code}")
        {:ok, response}
      {:error, error = %HTTPoison.Error{reason: reason}} when reason in [:timeout, :connect_timeout, :closed, :nxdomain, :ehostunreach] ->
        Instrumenter.http_request(path, error, start)
        retry(crawler, path, options, {:error, error}, retries)
        #if retries > 4  do
        #  error(crawler, "HTTP TIMEOUT: (#{inspect reason} - max retries reached)")
        #  {:error, error}
        #else
        #  error(crawler, "HTTP TIMEOUT: (#{inspect reason} - retry #{inspect retries})" <> inspect(error))
        #  :timer.sleep(:crypto.rand_uniform(retries*1000, retries*2000))
        #  request(crawler, path, options, retries + 1)
        #end
      {:error, error} ->
        Instrumenter.http_request(path, error, start)
        error(crawler, "HTTP ERROR: #{inspect error}")
        {:error, error}
    end
  end

  defp retry(crawler, path, options, error, retries) do
    if retries > 2 do
      error(crawler, "HTTP ERROR (max retries reached): #{inspect error}")
      error
    else
      debug(crawler, "HTTP retry #{inspect retries}: #{inspect error}")
      :timer.sleep(:crypto.rand_uniform(retries*500, retries*1000))
      Instrumenter.retry_http_request()
      request(crawler, path, options, retries + 1)
    end
  end

  def debug(crawler, message) do
    domain = crawler.instance.domain
    Logger.debug "Crawler(#{inspect self()} ##{crawler.instance.id} #{domain}): #{message}"
  end
  def info(crawler, message) do
    domain = crawler.instance.domain
    Logger.info "Crawler(#{inspect self()} ##{crawler.instance.id} #{domain}): #{message}"
  end
  def warn(crawler, message) do
    domain = crawler.instance.domain
    Logger.warn "Crawler(#{inspect self()} ##{crawler.instance.id} #{domain}): #{message}"
  end
  def error(crawler, message) do
    domain = crawler.instance.domain
    Logger.error "Crawler(#{inspect self()} ##{crawler.instance.id} #{domain}): #{message}"
  end

  def downcase(nil), do: nil
  def downcase(s), do: String.downcase(s)

  defp apply_callbacks(state, modules, callback) do
    args = [state]
    Enum.reduce(modules, state, fn(module, state) ->
      debug(state, "callback #{inspect {module, callback}}")
      apply(module, callback, [state])
    end)
  end
  if Mix.env == :dev do
    defp apply_safe_callbacks(state, modules, callback) do
      apply_callbacks(state, modules, callback)
    end
  else
    defp apply_safe_callbacks(state, [], _), do: state
    defp apply_safe_callbacks(state, [module | modules], callback) do
      state = apply(module, callback, [state])
      apply_safe_callbacks(state, modules, callback)
    rescue
      e ->
        Sentry.capture_exception(e, [stacktrace: System.stacktrace(), extra: %{callback: to_string(callback), module:
            to_string(module), instance: state.instance.domain}])
        Logger.error "callback #{inspect {module,callback}} #{inspect(state.instance.id)} rescued: #{inspect e}"
        state
    catch
      e ->
        Sentry.capture_exception(e, [extra: %{callback: to_string(callback), module: to_string(module), instance: state.instance.domain}])
        Logger.error "callback #{inspect {module,callback}} #{inspect(state.instance.id)} catched: #{inspect e}"
        state
    end

  end


end
