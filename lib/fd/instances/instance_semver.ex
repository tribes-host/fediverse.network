defmodule Fd.Instances.Semver do
  use Ecto.Schema
  import Ecto.Changeset
  alias Fd.Instances.{Instance, InstanceSettings}

  @primary_key false
  embedded_schema do
    field :major, :integer
    field :minor, :integer
    field :patch, :integer
    field :pre, {:array, :string}
    field :build, {:array, :string}
    field :vsn, :string
    field :major_minor, :string
    field :normalized, :string
  end

  def changeset(semver = %__MODULE__{}, attrs) do
    semver
    |> cast(attrs, [:major, :minor, :patch, :pre, :build, :vsn, :major_minor, :normalized])
  end

end
