defmodule Fd.Instances.RestrictionRule do
  use Ecto.Schema
  import Ecto.Changeset
  alias __MODULE__

  @primary_key false

  @renames %{
    "suspend" => ["reject"],
    "silence" => ["federated_timeline_removal", "silence"],
    "reject_media" => ["media_removal"],
    "reject_reports" => ["reject_report"],
  }

  schema "instance_restriction_rules" do
    field :rule, :string
    belongs_to :instance, Fd.Instances.Instance
    belongs_to :peered_instance, Fd.Instances.Instance, foreign_key: :peered_instance_id
    timestamps(updated_at: false)
  end

  def changeset(%__MODULE__{} = rule, attrs) do
    rule
    |> cast(attrs, [:instance_id, :peered_instance_id, :rule])
  end

  for {key, rules} <- @renames do
    def expand(unquote(key)) do
      unquote(rules)
    end
  end

  def expand(key), do: [key]

end
