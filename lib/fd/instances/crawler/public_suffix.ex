defmodule Fd.Instances.Crawler.PublicSuffix do
  use Fd.Instances.Crawler

  def process_results(crawler) do
    crawler = unless crawler.instance.domain_suffix do
      debug(crawler, "setting domain suffix")
      domain = String.downcase(crawler.instance.domain)
      suffix = PublicSuffix.public_suffix(domain, ignore_private: true)
      changes = crawler.changes
      |> Map.put("domain_suffix", suffix)
      %Crawler{crawler | changes: changes}
    else
      crawler
    end

    crawler = unless crawler.instance.domain_base do
      debug(crawler, "setting domain base")
      domain = String.downcase(crawler.instance.domain)
      base = PublicSuffix.registrable_domain(domain)
      changes = crawler.changes
      |> Map.put("domain_base", base)
      %Crawler{crawler | changes: changes}
    else
      crawler
    end
  end

end
