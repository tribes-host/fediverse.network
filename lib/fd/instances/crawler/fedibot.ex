defmodule Fd.Instances.Crawler.Fedibot do
  use Fd.Instances.Crawler

  def after_run(crawler = %{diffs: diffs}) do
    unless !Application.get_env(:fd, :fedibot_enabled, false) && Instance.hidden?(crawler.instance) or Map.get(crawler.changes, "server") == 0 do
      if diffs.became_up? do
        post("{instance} is back up :)", crawler.instance, [:mon])
      end
      if diffs.became_down? do
        error = if error = Map.get(crawler.check, "error_s") do
          " (#{error})"
        else
          ""
        end
        if Map.get(crawler.instance.settings || %{}, :maintenance_mode) do
          post("{instance} is undergoing planned maintenance#{error}", crawler.instance, [:mon])
        else
          post("{instance} is down#{error}", crawler.instance, [:mon])
        end
      end
      if diffs.became_closed? do
        post("{instance} closed registrations", crawler.instance, [:watch, :mon])
      end
      if diffs.became_open? do
        post("{instance} opened registrations", crawler.instance, [:watch, :mon])
      end
      if diffs.new? do
        server_id = Map.get(crawler.changes, "server", 0)
        unless server_id == 0 do
          server = server_id |> Fd.ServerName.from_int()
          [
            "{instance}, welcome to the fediverse! a new {server} instance! \\o/",
            "\\o/ please welcome {instance} to the fediverse, a new {server} instance",
            "one more {server} instance in the fediverse! welcome, {instance}!"
          ]
          |> Enum.random()
          |> post(crawler.instance, [:new, :watch, :mon], %{server: server})
        end
      end
      cond do
        diffs.server_changed? ->
          last = diffs.last_up_check.server || 0
          unless last == 0 do
            old_server = diffs.last_up_check.server |> Fd.ServerName.from_int()
            new_server = Map.get(crawler.changes, "server", 0) |> Fd.ServerName.from_int()
            post("{instance} changed servers from #{old_server} to #{new_server}", crawler.instance, [:watch, :mon])
          end
        diffs.version_changed? ->
          server = Map.get(crawler.changes, "server", 0) |> Fd.ServerName.from_int()
          old_version = diffs.last_up_check.version
          new_version = Map.get(crawler.changes, "version", "?")
          post("{instance} upgraded #{server} from #{old_version} to #{new_version}:", crawler.instance, [:watch, :mon])
        true -> :nothing_changed
      end
    end
    crawler
  end

  defp post(text, instance, accounts, replaces \\ %{}) do
    Logger.warn inspect(instance.settings)
    accounts = if Map.get(instance.settings || %{}, :fedibot) do
      [instance.domain] ++ accounts
    else
      accounts
    end

    {instance_domain, link} = if Instance.hidden?(instance) do
      {"[private]", nil}
    else
      {Fd.Util.idna(instance.domain), "https://fediverse.network/#{Fd.Util.idna(instance.domain)}"}
    end

    text = text
    |> String.replace("{instance}", instance_domain)
    |> (fn(text) ->
      Enum.reduce(replaces, text, fn({replace, with_text}, text) ->
        String.replace(text, "{#{replace}}", with_text)
      end)
    end).()
    |> (fn(text) ->
      [text, link]
    end).()
    |> Enum.filter(&(&1))
    |> Enum.join(" - ")

    Fd.Pleroma.post(accounts, text)
  end

end
