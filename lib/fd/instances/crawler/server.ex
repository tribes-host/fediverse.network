defmodule Fd.Instances.Crawler.Server do
  use Fd.Instances.Crawler

  @moduledoc """
  Server-specific callbacks modules for the crawler pipeline
  """

  @servers %{
    2 => __MODULE__.Mastodon,
    3 => __MODULE__.Pleroma
  }
  @server_ids Map.keys(@servers)

  def process_results(crawler = %Crawler{changes: %{"server" => server_id}}) when server_id in @server_ids do
    module = Map.get(@servers, server_id)
    module.process_results(crawler)
  end

  def process_results(crawler) do
    crawler
  end

  def after_run(crawler = %Crawler{changes: %{"server" => server_id}}) when server_id in @server_ids do
    module = Map.get(@servers, server_id)
    module.after_run(crawler)
  end

  def after_run(crawler) do
    crawler
  end

end
