defmodule Fd.Instances.Crawler.Nodeinfo do
  use Fd.Instances.Crawler

  @doc "nodeinfo"

  @versions ["1.0", "1.1", "2.0", "2.1"]
  @schemas Enum.reduce(@versions, %{}, fn(version, schemas) ->
    schema = File.read!("priv/json_schemas/nodeinfo_#{version}.json")
    |> Jason.decode!()
    |> ExJsonSchema.Schema.resolve()
    Map.put(schemas, version, schema)
  end)
  @well_known_schema File.read!("priv/json_schemas/nodeinfo_well_known.json")
  |> Jason.decode!()
  |> ExJsonSchema.Schema.resolve()

  #def query_nodeinfo(crawler = %Crawler{halted?: false, s_config: %{"site" => %{"platform" => %{"PLATFORM_NAME" => server}}}}) when server in @nodeinfo_servers, do: do_query_nodeinfo(crawler)
  #def query_nodeinfo(crawler = %Crawler{halted?: false, s_config: %{"site" => %{"friendica" => _}}}), do: do_query_nodeinfo(crawler)
  def query(crawler = %Crawler{has_peertubeapi?: true}), do: crawler
  def query(crawler = %Crawler{halted?: false}), do: query_well_known(crawler)
  def query(crawler), do: crawler

  def process_results(crawler = %{has_nodeinfo?: true}) do
    debug(crawler, "querying nodeinfo :)")
    users = get_in(crawler.nodeinfo, ["usage", "users", "total"])
    posts = get_in(crawler.nodeinfo, ["usage", "localPosts"])
    comments = get_in(crawler.nodeinfo, ["usage", "localComments"])
    server = Fd.ServerName.to_int(get_in(crawler.nodeinfo, ["software", "name"])||0)
    version = get_in(crawler.nodeinfo, ["software", "version"])
    signup = get_in(crawler.nodeinfo, ["openRegistrations"])

    metadata = get_in(crawler.nodeinfo, ["metadata"]) || %{}
    name = Map.get(metadata, "nodeName")
    description = Map.get(metadata, "description")
    email = Map.get(metadata, "email")
    private = Map.get(metadata, "private")
    protocols = case Map.get(crawler.nodeinfo || %{}, "protocols") do
      list when is_list(list) -> list
      _ -> []
    end
    |> Enum.filter(fn(x) -> x end)
    |> Enum.map(fn(proto) -> String.downcase(proto) end)
    |> Enum.uniq()

    features = case Map.get(metadata, "features") do
      list when is_list(list) -> list
      _ -> []
    end
    |> Enum.filter(fn(x) -> x end)
    |> Enum.map(fn(proto) -> String.downcase(proto) end)
    |> Enum.uniq()

    toint = fn(int) when is_integer(int) -> int
              (string) when is_binary(string) ->
      case Integer.parse(string) do
        {int, _} -> int
        :error -> 0
      end
    end

    statuses = cond do
      posts && comments -> toint.(posts) + toint.(comments)
      is_integer(posts) -> toint.(posts)
      is_integer(comments) -> toint.(comments)
      true -> nil
    end

    check = (crawler.check || %{})
    |> Crawler.put("up", true)
    |> Crawler.put_new("users", users)
    |> Crawler.put_new("statuses", statuses)
    |> Crawler.put_new("server", server)
    |> Crawler.put_new("version", version)
    |> Crawler.put_new("signup", signup)
    |> Crawler.put_new("protocols", protocols)
    |> Crawler.put_new("features", features)

    changes = (crawler.changes || %{})
    |> Crawler.put("nodeinfo", crawler.nodeinfo)
    |> Crawler.put("name", name)
    |> Crawler.put("description", description)
    |> Crawler.put("email", email)
    |> Crawler.put("last_up_at", DateTime.utc_now())
    |> Crawler.put("hidden", private)
    |> Crawler.put("dead", false)
    |> Map.merge(check)

debug(crawler,"processed nodeinfo ! #{inspect changes}")

    %Crawler{crawler | changes: changes, check: check}
  end

  def process_results(crawler), do: crawler

  @not_found [404]
  @down_http_codes Crawler.down_http_codes()

  def query_well_known(crawler, options \\ []) do
    always_halt = Keyword.get(options, :always_halt, false)

    case request(crawler, "/.well-known/nodeinfo", [accept: "application/jrd+json, application/json"]) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        debug(crawler, "got /.well-known/nodeinfo " <> inspect(body))

        body = case ExJsonSchema.Validator.validate(@well_known_schema, body) do
          :ok ->
            body
            |> Map.put("__schema_valid?", true)
          {:error, errors} ->
            body
            |> Map.put("__schema_valid?", false)
            |> Map.put("__schema_errors", format_errors(errors))
        end

        %Crawler{crawler | has_nodeinfo?: true, nodeinfo_schema: body}
        |> query_nodeinfo(options)
      {:ok, %HTTPoison.Response{status_code: code}} when code in @not_found ->
        debug(crawler, ".well-known/nodeinfo is not found. #{inspect code}")
        %Crawler{crawler | halted?: always_halt, has_nodeinfo?: false, nodeinfo: %{"error" => "/.well-known/nodeinfo not found #{code}"} }
      {:ok, %HTTPoison.Response{status_code: code}} when code not in @down_http_codes  ->
        debug(crawler, "nodeinfo well-known responded with an invalid code, maybe down or not found: #{inspect code}")
        %Crawler{crawler | halted?: always_halt, has_nodeinfo?: false, nodeinfo: %{"error" => "/.well-known/nodeinfo bad http code #{code}"}}
      {:error, %Jason.DecodeError{}} ->
        debug(crawler, "nodeinfo json decode error, skipping")
        %Crawler{crawler | halted?: always_halt, has_nodeinfo?: false, nodeinfo: %{"error" => "/.well-known/nodeinfo invalid json"}}
      failed ->
        debug(crawler, "host is down " <> inspect(failed))
        %Crawler{crawler | halted?: true, fatal_error: failed, nodeinfo: %{"error" => "host down #{inspect failed}"}}
    end
  end

  defp query_nodeinfo(crawler) do
    query_nodeinfo(crawler, [])
  end

  # Fix prismo buggy well known
  #  defp query_nodeinfo(crawler = %Crawler{has_nodeinfo?: true, nodeinfo_schema: schema = %{"links" => [%{"rel" => rel},
        #      %{"href" => href}]}}, _) do
    #  IO.inspect("fixshit")
    #   query_nodeinfo(%Crawler{crawler | nodeinfo_schema: %{schema | "links" => [%{"rel" => rel, "href" => href}]}})
    #end

  defp query_nodeinfo(crawler = %Crawler{has_nodeinfo?: true, nodeinfo_schema: %{"links" => schemas}}, options) do
    schemas = case Enum.into(List.first(schemas) || %{}, []) do
      [{"rel", rel}] ->
        [%{"rel" => rel}, %{"href" => href}] = schemas
        [%{"href" => href, "rel" => rel}]
      _ -> schemas
    end
    links = Enum.reduce(schemas, [], fn(schema, acc) ->
      href = Map.get(schema, "href", "")
      uri = URI.parse(href)
      version = detect_version(Map.get(schema, "rel"))
      cond do
        version && uri.host == crawler.instance.domain -> [{version, uri.path} | acc]
        true -> acc
      end
    end)
    |> Enum.sort_by(fn({v, _}) -> v end, &>=/2)

    crawl_links = if Keyword.get(options, :get_all, false) do
      links
    else
      List.first(links)
    end
    do_query_nodeinfo(crawler, crawl_links)
  end

  defp query_nodeinfo(crawler, options) do
    %Crawler{crawler | has_nodeinfo?: false}
  end

  defp do_query_nodeinfo(crawler, list) when is_list(list) do
    all = Enum.reduce(list, %{}, fn({version, path}, acc) ->
      crw = do_query_nodeinfo(crawler, {version, path})
      |> Map.from_struct
      |> Map.take([:halted?, :has_nodeinfo?, :nodeinfo])

      Map.put(acc, version, crw)
    end)

    halted = Enum.all?(all, fn({_,x}) -> x.halted? end)
    valid = Enum.all?(all, fn({_,x}) -> Map.get((x.nodeinfo || %{}), "__schema_valid?", false) end)
    has_nodeinfo = Enum.any?(all, fn({_,x}) -> x.has_nodeinfo? end)

    nodeinfo = Map.new
    |> Map.put(:results, all)
    |> Map.put(:has_nodeinfo, has_nodeinfo)
    |> Map.put(:all_valid?, valid)
    |> Map.put(:halted?, halted)

    %Crawler{crawler | halted?: halted, has_nodeinfo?: has_nodeinfo, nodeinfo: nodeinfo}
  end

  defp do_query_nodeinfo(crawler, {version, path}) do
    debug(crawler, "Should crawl Nodeinfo ver #{inspect version} at path #{inspect path}")
    case request(crawler, path) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        debug(crawler, "got nodeinfo#{inspect version} #{inspect path} " <> inspect(body))

        # REMOVE WHEN Wordpress nodeinfo is fixed: https://github.com/pfefferle/wordpress-nodeinfo/issues/1
        body = if is_map(Map.get(body, "metadata")) do
          body
        else
          Map.put(body, "metadata", %{})
        end

        body = if Map.get(@schemas, to_string(version)) do
          case ExJsonSchema.Validator.validate(Map.get(@schemas, to_string(version)), body) do
            :ok ->
              body
              |> Crawler.put("__schema_valid?", true)
            {:error, errors} ->
              body
              |> Map.put("__schema_valid?", false)
              |> Map.put("__schema_errors", format_errors(errors))
          end
        else
          body
          |> Map.put("__schema_valid?", false)
          |> Map.put("__schema_errors", format_errors([{"Unsupported nodeinfo schema version", to_string(version)}]))
        end
        |> Map.put("__path", path)
        |> Map.put("__version", version)

        %Crawler{crawler | has_nodeinfo?: true, nodeinfo: body}
      {:ok, %HTTPoison.Response{status_code: code}} when code in @not_found ->
        debug(crawler, "nodeinfo #{path} is not found. #{inspect code}")
        %Crawler{crawler | has_nodeinfo?: false}
      {:ok, %HTTPoison.Response{status_code: code}} when code not in @down_http_codes  ->
        debug(crawler, "nodeinfo #{path} responded with an invalid code, maybe down or not found: #{inspect code}")
        crawler
      {:error, %Jason.DecodeError{}} ->
        debug(crawler, "nodeinfo #{path} json decode error, skipping")
        crawler
      failed ->
        debug(crawler, "host is down " <> inspect(failed))
        %Crawler{crawler | halted?: true, fatal_error: failed}
    end
  end

  defp do_query_nodeinfo(crawler, nil) do
    debug(crawler, "no valid nodeinfo")
    %Crawler{crawler | halted?: true, fatal_error: :invalid_nodeinfo}
  end

  defp do_query_nodeinfo(crawler, _) do
    %Crawler{crawler | has_nodeinfo?: false}
  end

  defp detect_version("http://nodeinfo.diaspora.software/ns/schema/"<>float) do
    case Float.parse(float) do
      {version, _} -> version
      _ -> nil
    end
  end

  defp detect_version(_), do: nil

  defp format_errors(errors) do
    Enum.reduce(errors, Map.new, fn({error, field}, acc) ->
      facc = Map.get(acc, field, [])
      Map.put(acc, field, [error | facc])
    end)
  end

end
