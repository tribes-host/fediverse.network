defmodule Fd.Instances.Crawler.Peers do
  use Fd.Instances.Crawler

  @doc "peer relations"

  def after_run(crawler) do
    spawn(__MODULE__, :crawl_peers, [crawler])
    crawler
  end

  def crawl_peers(crawler) do
    Logger.info "running peers"
    domains = crawler.m_peers || []
    |> Enum.uniq()

    existings = Enum.map(Fd.Instances.list_instances_by_peer_domains(domains), fn(i) -> i end)
    existing_domains = Enum.map(existings, &String.downcase(&1.domain))

    new_domains = domains
    |> Enum.filter(fn(domain) -> domain end)
    |> Enum.map(fn(domain) ->
      domain = domain
      |> String.trim
      |> String.downcase
      uri = URI.parse("https://#{domain}")
      uri.host
    end)
    |> Enum.filter(fn(domain) -> domain end)
    |> Enum.reject(fn(domain) -> Enum.member?(existing_domains, domain) end)

    # Create new instances
    created = for domain <- new_domains do
      case Fd.Instances.create_instance(%{"domain" => domain}) do
        {:ok, instance} -> instance
        _ -> nil
      end
    end
    |> Enum.filter(fn(i) -> i end)

    # Upsert peers relations
    relations = Enum.map(existings ++ created, fn(peer) ->
        %{instance_id: crawler.instance.id, peered_instance_id: peer.id, inserted_at: DateTime.utc_now()}
    end)
    inserted = Fd.Repo.insert_all("instance_peers", relations, on_conflict: :nothing)
    Logger.debug "Added #{inspect inserted} entries in instances_peers"

    # Remove removed peers
    peer_ids = Enum.map(existings ++ created, fn(i) -> i.id end)
    id = crawler.instance.id
    import Ecto.Query
    removed = from(rel in "instance_peers",
      where: rel.instance_id == ^id and rel.peered_instance_id not in ^peer_ids)
    |> Fd.Repo.delete_all()
    Logger.debug "Removed #{inspect removed} entries from instance_peers."

    # Start new instances
    for instance <- created do
       if Application.get_env(:fd, :instances)[:autostart] do
          spawn(fn() ->
            :timer.sleep(:rand.uniform(60) * 1000)
            Fd.Instances.Server.crawl(instance.id)
          end)
      end
    end
  end


end
