defmodule Fd.Instances.Crawler.Timeline do
  require Logger
  alias Fd.{Repo, Instances, Instances.Instance}
  @behavior :gen_statem

  @moduledoc """
  Side instance server that connects to timeline.

  Doing this allows to:
    * better activity aggregates
    * better unique user count aggregates
    * hashtag activity aggregates
    * ? more aggregates, probably!

  Is _only ever_ stored:
    * hash of original post
    * hash of user profile link
    * language
    * originating instance
    * instances that has seen the message:
      * + the delay between post time and reception time
      * + the local id
    * hashtags

  Deleted messages are honored, but to preserve statistics, the entry remain with:
    * user hash replaced by unique random value
    * time is cropped

  For now on this is an alpha feature for research.

  It uses a `gun` http connection, will try using SSE first, then websocket, then maybe polling.
  """

  @min_backoff :timer.seconds(5)
  @max_backoff :timer.minutes(20)
  @save_tick :timer.seconds(5)
  @await_up :timer.minutes(2)
  @await_stream :timer.minutes(3)
  @read_timeout :timer.minutes(3)

  def start(domain) when is_binary(domain) do
    Fd.Instances.get_instance_by_domain!(domain)
    |> start()
  end

  def start(instance = %Instance{}) do
    pid = :gen_statem.start(process_name(instance), __MODULE__, [instance], [])
    #:gen_statem.start(__MODULE__, [instance], [name: {:global, "instance:tl:#{to_string(instance.id)}"}])
  end

  def pid(instance_or_id) do
    :global.whereis_name(process_name(instance_or_id))
  end

  def control(instance_or_id, message) do
    msg = {:control, message}
    case pid(instance_or_id) do
      :undefined ->
        :not_started
        #GenServer.call(pid, msg)
      pid ->
        GenServer.call(pid, msg)
    end
  end

  # -- FSM

  def callback_mode, do: :state_functions

  def init([instance]) do
    Logger.debug "starting instance tlfsm #{inspect instance.domain}"
    Logger.metadata([request_id: "tl:#{instance.id}:#{instance.domain}"])
    state = %{
      id: instance.id,
      instance: instance,
      conn: nil,
      backoff: :backoff.init(@min_backoff, @max_backoff),
      stream: nil,
      buffer: <<>>,
      save_buffer: [],
      save_h_buffer: [],
      ws: false,
      poll: false,
      poll_state: %{},
    }
    {:ok, :not_connected, state, [{:next_event, :internal, nil}]}
    #{:ok, state, {:continue, :connect}}
  end

  def not_connected(i, _, state) when i in [:internal, :state_timeout] do
    # http1 is forced because of websocket fallback: is there really a reason to fallback to websocket?.
    if state.conn do
      :gun.close(state.conn)
    end
    case :gun.open(to_char_list(state.instance.domain), 443, %{transport: :tls, protocols: [:http], retry: 0}) do
      {:ok, conn} ->
        {_, backoff} = :backoff.succeed(state.backoff)
        Process.monitor(conn)
        {:next_state, :connecting, %{state | backoff: backoff, conn: conn}, {:state_timeout, @await_up, nil}}
      {:error, error} ->
        {delay, backoff} = :backoff.fail(state.backoff)
        Logger.warn("Error connecting: #{inspect error}")
        {:keep_state, %{state | backoff: backoff, conn: nil}, {:state_timeout, delay, nil}}
    end
  end

  def not_connected(type, event, state) do
    Logger.error "not_connected #{inspect {type,event,state}}"
    handle_common(type, event, state)
  end

  def connecting(:state_timeout, _, state) do
    Logger.debug "connecting: state_timeout"
    {delay, backoff} = :backoff.fail(state.backoff)
    {:next_state, :not_connected, %{state | backoff: backoff, conn: nil}, {:state_timeout, delay, nil}}
  end

  def connecting(:info, {:gun_up, conn, _}, state = %{conn: conn}) do
    Logger.debug "connecting: up!"
    {_, backoff} = :backoff.succeed(state.backoff)
    {:next_state, :connected, %{state | backoff: backoff}, {:next_event, :internal, nil}}
  end

  def connecting(type, event, state) do
    Logger.error "connected #{inspect {type,event,state}}"
    handle_common(type, event, state)
  end

  def connected(:state_timeout, :timeout, state) do
    {delay, backoff} = :backoff.fail(state.backoff)
    {:next_state, :not_connected, %{state | backoff: backoff}, {:state_timeout, delay, :timeout}}
  end

  def connected(i, _, state = %{conn: conn}) when i in [:internal, :state_timeout] do
    headers = [
      {"accept", "text/event-stream"},
      {"user-agent", Fd.user_agent("experimental crawler")}
    ]
    stream = cond do
      state.ws ->
        Logger.debug("connected: connected, opening ws upgrade")
        :gun.ws_upgrade(state.conn, to_char_list("/api/v1/streaming?stream=public"), headers)
      state.poll ->
        Logger.debug("connected: connected, polling")
        :gun.get(state.conn, poll_url(state), headers)
      true ->
        Logger.debug("connected: connected, opening sse stream")
        :gun.get(state.conn, to_char_list("/api/v1/streaming/public"), headers)
    end
    Logger.debug("State is: #{inspect stream}")
    {:next_state, :opening, %{state | stream: stream}, {:state_timeout, @await_stream, :timeout}}
  end

  def connected(type, event, state) do
    Logger.error "connected #{inspect {type,event,state}}"
    handle_common(type, event, state)
  end


  def opening(:state_timeout, backoff, state) do
    Logger.debug "opening: state_timeout"
    {delay, backoff} = :backoff.fail(state.backoff)
    {:next_state, :connected, %{state | backoff: backoff, stream: nil}, {:state_timeout, delay, nil}}
  end

  def opening(:info, {:gun_data, _, _, _, _}, state) do
    Logger.debug("Ignoring gun data in opening")
    {:next_state, :opening, state, {:state_timeout, @await_stream, nil}}
  end

  # Got a 200 in Poll
  def opening(:info, {:gun_response, conn, stream, :nofin, 200, headers}, state = %{poll: true}) do
    Logger.debug "opening resp ok"
    {_, backoff} = :backoff.succeed(state.backoff)
    {:next_state, :opened, %{state | backoff: backoff, stream: stream}, {:state_timeout, @read_timeout, :timeout}}
  end

  # Got a 200 in SSE!
  def opening(:info, {:gun_response, conn, stream, :nofin, 200, headers}, state) do
    Logger.debug "opening resp ok"
    {_, backoff} = :backoff.succeed(state.backoff)
    {:next_state, :opened, %{state | backoff: backoff, stream: stream}, {:state_timeout, @read_timeout, :timeout}}
  end

  # Got a WS Upgrade!
  def opening(:info, {:gun_upgrade, conn, stream, ["websocket"], headers}, state) do
    Logger.debug "opening: upgrade ok"
    {_, backoff} = :backoff.succeed(state.backoff)
    {:next_state, :opened, %{state | backoff: backoff, stream: stream, ws: true}, {:state_timeout, @read_timeout, :timeout}}
  end

  # SSE does not seem to work-- Switch to websocket now
  # 527 is returned by cloudflare stuff when they throw away sse/websocket in buffers or something and then timeout
  def opening(:info, {:gun_response, conn, stream, _, error, headers}, state = %{ws: false, poll: false}) when error in [200, 404, 527] do
    Logger.debug "opening: response code #{to_string(error)} -- trying websocket"
    {:next_state, :connected, %{state | stream: nil, ws: true}, {:next_event, :internal, nil}}
  end

  # Websocket failed for some reasons? Try polling
  def opening(:info, {:gun_response, conn, stream, _, error, headers}, state = %{ws: true}) when error < 500 or error == 527 do
    Logger.debug "opening: response code #{to_string(error)} -- trying polling"
    {:next_state, :connected, %{state | stream: nil, ws: false, poll: true}, {:next_event, :internal, nil}}
  end

  # 5xx, trigger reconnect
  def opening(:info, {:gun_response, conn, stream, _, error, headers}, state) when error >= 500 do
    Logger.debug "opening: response code #{to_string(error)}"
    {delay, backoff} = :backoff.fail(state.backoff)
    {:next_state, :connected, %{state | backoff: nil, stream: nil, ws: false}, {:state_timeout, delay, nil}}
  end

  # Bad response -- stop now
  def opening(:info, {:gun_response, conn, stream, _, error, headers}, state) do
    Logger.debug "opening: bad response code #{to_string(error)} -- stopping."
    {:stop, :normal}
  end

  def opening(:info, {:gun_error, conn, stream, error}, state) do
    Logger.warn "opening: gun error #{inspect(error)}"
    {delay, backoff} = :backoff.fail(state.backoff)
    {:next_state, :connected, %{state | backoff: backoff, stream: nil, ws: false}, {:state_timeout, delay, nil}}
  end

  def opening(type, event, state) do
    Logger.error "openin #{inspect {type,event,state}}"
    handle_common(type, event, state)
  end

  @open_timeouts [{:state_timeout, @read_timeout, :timeout}, {:timeout, @save_tick, :save}]

  # SSE data
  def opened(:info, {:gun_data, conn, stream, :nofin, data}, state = %{conn: conn, stream: stream}) do
    state = if length(state.save_buffer) > 20, do: save(state), else: state
    if String.ends_with?(data, "\n") do
      data = state.buffer <> data
      |> String.trim()
      |> String.split("\n")

      state = %{state | buffer: <<>>}

      state = case data do
        ["event: "<>event, "data: "<>data] ->
          safe_wrap(state, fn() -> process_event(state, event, data) end)
        [":thump"] ->
          save(state)
        other ->
          Logger.error "Malformated message: #{inspect other}"
          state
      end
      {:keep_state, state, @open_timeouts}
    else
      {:keep_state, %{state | buffer: state.buffer <> data}, @open_timeouts}
    end
  end

  # WS ping
  def opened(:info, {:gun_ws, conn, stream, {:text, ""}}, state = %{conn: conn, stream: stream}) do
    {:keep_state, save(state), @open_timeouts}
  end

  # WS data
  def opened(:info, {:gun_ws, conn, stream, {:text, json}}, state) do
    state = if length(state.save_buffer) > 20, do: save(state), else: state
    with \
      {:ok, data} <- Jason.decode(json),
      event <- Map.get(data, "event"),
      payload <- Map.get(data, "payload")
    do
      {:keep_state, safe_wrap(state, fn() -> process_event(state, event, payload) end), @open_timeouts}
    else
      error ->
        Logger.error "Error in WS msg #{inspect error}"
        {:keep_state, state, @open_timeouts}
    end
  end

  def opened(:timeout, :save, state) do
    Logger.debug "opened: save timeout"
    {:keep_state, save(state), @open_timeouts}
  end

  def opened(:state_timeout, :timeout, state) do
    Logger.debug "opened: state_timeout"
    {:next_state, :connected, state}
  end

  def opened(type, event, state) do
    Logger.error "openin #{inspect {type,event,state}}"
    handle_common(type, event, state)
  end

  def code_change(_vsn, state, data, _extra) do
    {:ok, state, data}
  end

  # The Gun connection went down.
  def handle_common(:info, {:gun_down, conn, _protocol, reason, killed_streams, unprocessed_streams}, state) do
    Logger.error "Connection down: #{inspect reason}"
    {delay, backoff} = :backoff.fail(state.backoff)
    {:next_state, :not_connected, %{state | backoff: backoff, conn: nil, stream: nil}, {:state_timeout, delay, nil}}
  end

  # The stream errored
  def handle_common(:info, {:gun_error, conn, stream, reason}, state = %{conn: conn, stream: stream}) do
    Logger.error "Stream error: #{inspect reason}"
    {delay, backoff} = :backoff.fail(state.backoff)
    {:next_state, :connected, %{state | stream: nil}, {:state_timeout, delay, nil}}
  end

  # The connection errored
  def handle_common(:info, {:gun_error, conn, reason}, state = %{conn: conn}) do
    Logger.error "Connection error: #{inspect reason}"
    {:keep_state, state}
  end

  def handle_common(type, event, state) do
    {:keep_state, state}
  end

  def terminate(reason, state, data) do
    Logger.error("Terminating in #{inspect state} - #{inspect reason}")
    :ok
  end

  defp process_event(state, "update", json) do
    with \
      {:ok, data} <- Jason.decode(json),
      id when not is_nil(id) <- Map.get(data, "id"),
      status_url when not is_nil(status_url) <- Map.get(data, "uri") || Map.get(data, "url"),

      account_url when not is_nil(account_url) <- get_in(data, ["account", "url"]),
      date_string when not is_nil(date_string) <- Map.get(data, "created_at"),
      lang <- Map.get(data, "language"),
      uhash <- :erlang.phash2(account_url),
      ihash <- :erlang.phash2(status_url),
      {:ok, at} <- NaiveDateTime.from_iso8601(date_string),
      now <- NaiveDateTime.utc_now(),
      %URI{host: host} when not is_nil(host) <- URI.parse(status_url),
      {:ok, instance_id} <- Fd.Instances.Lookup.get_by_domain(host),
      tags <- Enum.map(Map.get(data, "tags", []), fn(tag) -> Map.get(tag, "name") end)
    do
      event = %{
        "uhash" => uhash,
        "ihash" => ihash,
        "created_at" => at,
        "updated_at" => now,
        "instance_id" => instance_id,
        "seen_by_instance_id" => state.instance.id,
        "lang" => lang,
        "local_id" => id
      }

      tags = Enum.map(tags, fn(tag) ->
        %{"tag" => tag, "ihash" => ihash, "created_at" => at, "instance_id" => instance_id, "lang" => lang}
      end)

      %{state | save_buffer: [event | state.save_buffer], save_h_buffer: [tags | state.save_h_buffer]}
    else
      error ->
        Logger.error("Update event parse error #{inspect error}")
        state
    end
  end

  defp process_event(state, "delete", id) do
    # 1. Find statuses row where local_id=id seen_by_instance_id=state.instance.id
    # 2. Update statuses where ihash = row.ihash, uhash = null, ihash = randomize, local_id = null
    # 3. :)
    state
  end

  defp process_event(state, event, data) do
    Logger.debug "Unsupported WS event: #{inspect {event, data}}"
    state
  end

  defp save(state) do
    safe_wrap(state, fn() ->
      inserted = unless Enum.empty?(state.save_buffer) do
        s = Fd.Repo.insert_all("statuses", state.save_buffer, on_conflict: :nothing)
        h = Fd.Repo.insert_all("hashtags", List.flatten(state.save_h_buffer), on_conflict: :nothing)
        {s, h}
      else
        "empty"
      end
      Logger.debug "save tick: #{inspect inserted}"
      %{state | save_buffer: [], save_h_buffer: []}
    end)
  end

  # -- poll helpers
  defp poll_url(state = %{poll_state: poll_state}) do
    params = %{"local" => true, "limit" => 100}
    params = if last = Map.get(poll_state, "last") do
      Map.put(params, "min_id", last)
    else
      params
    end
    param_string = :cow_qs.qs(Enum.into(params, []))
    "/api/v1/timelines/public?#{param_string}"
    |> to_char_list()
  end


  def process_name(instance = %Instance{}) do
    process_name(instance.id)
  end
  def process_name(instance_id) do
    {:global, "instance:tl:#{to_string(instance_id)}"}
  end

  if @dev do
    def safe_wrap(_, fun) do
      fun.()
    end
  else
    def safe_wrap(state, fun) do
      fun.()
    rescue
      e ->
        Sentry.capture_exception(e, [stacktrace: System.stacktrace(), extra: %{instance: state.instance.domain}])
      Logger.error "TLServer #{inspect(state.id)} rescued: #{inspect e}"
    catch
      e ->
        Sentry.capture_exception(e, [extra: %{instance: state.instance.domain}])
        Logger.error "TLServer #{inspect(state.id)} catched: #{inspect e}"
    end
  end

end
