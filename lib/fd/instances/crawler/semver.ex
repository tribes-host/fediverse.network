defmodule Fd.Instances.Crawler.Semver do
  use Fd.Instances.Crawler

  def process_results(crawler = %Crawler{changes: changes}) do
    semver = with \
      string when is_binary(string) <- Map.get(changes, "version"),
      {:ok, version} <- Fd.Version.parse(string)
    do
      Fd.Version.to_instance_semver_params(version)
    else
      _ -> nil
    end

    changes = Map.put(changes, "semver", semver)
    %Crawler{crawler | changes: changes}
  end

end

