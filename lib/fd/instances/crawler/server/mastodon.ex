defmodule Fd.Instances.Crawler.Server.Mastodon do
  use Fd.Instances.Crawler

  def process_results(crawler) do
    crawler
    |> perform(:process_results, :nodeinfo)
  end

  def after_run(crawler) do
    crawler
    |> perform(:after_run, :nodeinfo)
  end

  # banana.dog PR
  # https://github.com/tootsuite/mastodon/pull/10079
  # FIXME: contains typo (metaData instead of metadata), invalid nodeinfo.
  defp perform(crawler = %{nodeinfo: nodeinfo = %{"metaData" => metaData}}, :process_results, :nodeinfo) do
    nodeinfo
    |> Map.drop(["metaData"])
    |> Map.put("metadata", metaData)

    perform(%Crawler{crawler | nodeinfo: nodeinfo}, :process_results, :nodeinfo)
  end

  defp perform(crawler = %{nodeinfo: nodeinfo = %{"metadata" => %{"federation" => %{}}}}, :process_results, :nodeinfo) do
    changes = crawler.changes
    |> Map.put("restrictions_disclosed", true)
    %Crawler{crawler | changes: changes}
  end

  defp perform(crawler = %{nodeinfo: nodeinfo = %{"metadata" => %{"federation" => rules = %{}}}}, :after_run, :nodeinfo) do
    Fd.Instances.update_instance_restrictions(crawler.instance, rules)
    crawler
  end

  defp perform(crawler, _, _) do
    crawler
  end

end
