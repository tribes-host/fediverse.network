defmodule Fd.Instances.Crawler.Server.Pleroma do
  use Fd.Instances.Crawler

  def process_results(crawler) do
    crawler
    |> perform(:process_results, :nodeinfo)
  end

  def after_run(crawler) do
    crawler
    |> perform(:after_run, :nodeinfo)
  end

  # Federation restrictions in nodeinfo
  defp perform(crawler = %{instance: %{nodeinfo: nodeinfo = %{"metadata" => %{"federation" => %{"mrf_policies" => _}}}}}, :process_results, :nodeinfo) do
    changes = crawler.changes
    |> Map.put("restrictions_disclosed", true)
    %Crawler{crawler | changes: changes}
  end

  defp perform(crawler = %{instance: %{nodeinfo: nodeinfo = %{"metadata" => %{"federation" => federation}}}}, :after_run, :nodeinfo) do
    rules = (Map.get(federation, "mrf_simple") || %{})
    |> Map.put("quarantined", Map.get(federation, "quarantined_instances") || [])
    Fd.Instances.update_instance_restrictions(crawler.instance, rules)
    crawler
  end

  defp perform(crawler, _, _) do
    crawler
  end

end
