defmodule Fd.Instances.Crawler.HostInfo do
  use Fd.Instances.Crawler

  @moduledoc "Extracts/formats hosting information (country, ISP, ...)"

  @isps %{
    "masto.host" => [
      "217.182.80.236",
      "2001:41d0:302:1100::1:7615",
      "178.33.220.142",
      "176.31.213.231",
      "54.38.247.97",
      "2001:41d0:302:1100::b:ace6",
    ],
    "Write.as" => [
      "50.56.249.138",
      "2001:4800:7812:514:500b:b07c:ff05:694d",
      "2001:4800:7810:512:500b:b07c:ff05:8178",
    ],
    "Write.as for Teams" => [
      "162.209.77.201",
      "2001:4800:7818:103:be76:4eff:fe01:76f9"
    ],
    "WriteFreely.host" => [
      "23.253.248.8",
      "2001:4800:7817:104:be76:4eff:fe04:49a2",
    ],
    "maas" => [
      "212.129.31.42"
    ]
  }
  |> Enum.reduce(%{}, fn({key, ips}, acc) ->
    Enum.reduce(ips, acc, fn(ip, acc) ->
      {:ok, ip} = :inet.parse_address(to_char_list(ip))
      Map.put(acc, ip, key)
    end)
  end)

  def process_results(state) do
    ips = ips(state.instance.domain)
    info = Enum.reduce(ips, %{asn: [], as_org: [], country: []}, fn(ip, acc) ->
      asn = case :locus.lookup(:asn, ip) do
        {:ok, asn} -> asn
        _ -> %{}
      end
      country = case :locus.lookup(:country, ip) do
        {:ok, country} -> country
        _ -> %{}
      end
      as_n = (get_in(asn, ["autonomous_system_number"]) || "") |> to_string()
      as_org = get_in(asn, ["autonomous_system_organization"])
      continent = (get_in(country, ["continent", "code"])||"") |> String.downcase()
      country = get_in(country, ["country", "iso_code"])
      isp = Map.get(@isps, ip, as_org)
      %{asn: [as_n | acc.asn], as_org: [isp | acc.as_org], country: [country |
          acc.country]}
    end)
    |> Enum.reduce(%{}, fn({key, value}, acc) ->
      value = value
      |> Enum.filter(fn(val) -> val && val != "" end)
      |> Enum.uniq()
      Map.put(acc, key, value)
    end)

    ips_string = Enum.map(ips, fn(ip) -> to_string(:inet.ntoa(ip)) end)

    changes = state.changes
    |> Map.put("ases", info.asn)
    |> Map.put("countries", info.country)
    |> Map.put("isps", info.as_org)
    |> Map.put("ips", ips_string)

    %{state | changes: changes}
  end

  def ips(domain) do
    [:inet, :inet6]
    |> Enum.map(fn(inet) -> :inet_res.gethostbyname(to_char_list(domain), inet) end)
    |> Enum.filter(fn({:ok, hostent}) -> true
      {:error, _} -> nil
    end)
    |> Enum.flat_map(fn({:ok, {:hostent, _, _, _, _, list}}) -> list
      _ -> nil
    end)
    |> Enum.filter(fn(x) -> x end)
    |> Enum.uniq()
  end

end
