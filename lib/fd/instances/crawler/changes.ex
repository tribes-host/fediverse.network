defmodule Fd.Instances.Crawler.Changes do
  use Fd.Instances.Crawler

  def process_results(crawler) do
    last_check = Instances.get_instance_last_check(crawler.instance)
    last_up_check = Instances.get_instance_last_up_check(crawler.instance)

    new? = (last_check == nil)
    is_up? = Map.get(crawler.changes, "up")
    was_up? = (last_check && last_check.up == true)
    became_up? = (is_up? == true && was_up? == false)
    became_down? = (is_up? == false && was_up? == true)
    signup_changed? = if last_up_check && !is_nil(last_up_check.signup) do
      last_up_check.signup != Map.get(crawler.changes, "signup", false)
    else
      false
    end

    version_changed? = if last_up_check && is_up? do
      Map.get(crawler.changes, "version") != last_up_check.version
    else false end
    server_changed? = if last_up_check && is_up? do
      Map.get(crawler.changes, "server", 0) != last_up_check.server
    else false end
    {became_open?, became_closed?} = cond do
      signup_changed? && Map.get(crawler.changes, "signup", true) == false ->
        {false, true}
      signup_changed? && Map.get(crawler.changes, "signup", false) == true ->
        {true, false}
      true ->
        {false, false}
    end

    diffs = %{new?: new?, became_up?: became_up?, became_down?: became_down?, version_changed?: version_changed?,
      server_changed?: server_changed?, is_up?: is_up?, was_up?: was_up?, became_open?: became_open?, became_closed?:
      became_closed?,
      last_up_check: last_up_check
    }
    debug(crawler, "Diffs: " <> inspect(diffs))

    %Crawler{crawler | diffs: diffs}
  end

end

