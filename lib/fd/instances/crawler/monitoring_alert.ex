defmodule Fd.Instances.Crawler.MonitoringAlert do
  use Fd.Instances.Crawler

  def after_run(crawler) do
    if Application.get_env(:fd, :monitoring_alerts, false) && crawler.instance.monitor && crawler.instance.settings && crawler.instance.settings.alerts_to_contact do
      spawn(fn() ->
        became_down? = Map.get(crawler.diffs, :became_down, false)
        became_up? = Map.get(crawler.diffs, :became_up, false)
        if became_down? do
          Fd.DownEmail.down_email(crawler.instance, crawler.check)
          |> Fd.Mailer.deliver()
        end
        if became_up? do
          Fd.UpEmail.up_email(crawler.instance)
          |> Fd.Mailer.deliver()
        end
      end)
    end
    crawler
  end

end

