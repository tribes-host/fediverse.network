defmodule Fd.Instances.Score do
  alias Fd.Instances.Instance

  @moduledoc """

  - size score: ( (statuses/fedi_total_statuses) + (users/fedi_total_users))
  - activity score:  ( ((new_statuses) + (new_users * 2)) / max_value)

  - score: activity score (rate of new users/statuses)
    & penalty: proportional size score
    & penalty: proportional to size score and isp score
    & penalty: proportional to reject score
    & penalty: proportional to recent downtime
    & penalty: proportional to how far away the instance major.minor version is to the latest visible version
    & bonus: instance is less than a week old, proportional to time (the newer the bigger)
    & bonus: proportional to the proportion of the server in the fedi (in the opposite, to bump fewer represented servers)
  """

  def activity(instance = %Instance{id: id}, type, period \\ "1h") do
    get_in(Fd.ActivityScore.get(), [period, id, type])
  end


  def score(instance = %Instance{id: id}, period \\ "1h") do
    0
  end

end
