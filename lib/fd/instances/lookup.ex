defmodule Fd.Instances.Lookup do
  @moduledoc "A caching server for no-queries instance lookups"
  use GenServer
  require Logger

  def add(%Fd.Instances.Instance{id: id, domain: domain}) do
    GenServer.call(__MODULE__, {:add, id, domain})
  end

  def get_by_domain(domain) do
    if id = Map.get(get(), domain) do
      {:ok, id}
    else
      {:error, :not_found}
    end
  end

  def get() do
    :persistent_term.get(__MODULE__)
  end

  def start_link() do
    GenServer.start_link(__MODULE__, [], [name: __MODULE__])
  end

  def init(_) do
    Logger.warn "Building instances lookup cache"
    data = Enum.reduce(Fd.Instances.list_instances, Map.new, fn(instance, acc) ->
      Map.put(acc, instance.domain, instance.id)
    end)
    :persistent_term.put(__MODULE__, data)
    {:ok, nil}
  end

  def handle_call({:add, id, domain}, _from, state) do
    update = :persistent_term.get(__MODULE__)
    |> Map.put(domain, id)

    :persistent_term.put(__MODULE__, update)
    {:reply, :ok, state}
  end

end

