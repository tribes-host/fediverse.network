defmodule Fd.Cldr do
  use Cldr,
    locales: ["en", "fr"],
    default_locale: "en",
    providers: [Cldr.Unit, Cldr.Number, Cldr.List]
end
