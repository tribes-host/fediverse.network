defmodule Fd.Version do
  defstruct [:original, :normalized, :major, :minor, :patch, {:pre, []}, {:build, []}]

  @moduledoc """
  Welcome to hell, also known as “a version parser for the whole fediverse“.

  Please use [SemVer](https://semver.org/) and make my life better.
  """

  import Kernel, except: [to_string: 1]

  def to_vsn_major_minor(%__MODULE__{major: major, minor: minor}) do
    %__MODULE__{major: major, minor: minor}
    |> to_string()
  end

  def to_vsn(%__MODULE__{major: major, minor: minor, patch: patch}) do
    %__MODULE__{major: major, minor: minor, patch: patch}
    |> to_string()
  end

  def to_instance_semver_params(version = %__MODULE__{}) do
    params = version
    |> Map.from_struct()
    |> Enum.map(fn({k, v}) -> {Kernel.to_string(k), v} end)
    |> Enum.into(Map.new)
    |> Map.put("vsn", to_vsn(version))
    |> Map.put("major_minor", to_vsn_major_minor(version))
  end

  def to_string(version = %__MODULE__{}) do
    version
    |> String.Chars.to_string()
  end

  @type t :: %__MODULE__{}

  @type opts :: []
  @spec parse(String.t()) :: Version.t()
  @spec parse(String.t(), opts()) :: Version.t()
  def parse(string, opts \\ [])

  def parse(original, opts) do
    string = translate(original, opts)
    case Version.Parser.parse_version(string, true) do
      # happy path, this is a good semver. thanks to whoever did that version string
      {:ok, {major, minor, patch, pre_parts, build_parts}} ->
        v = %__MODULE__{
          original: original, normalized: string,
          major: major, minor: minor, patch: patch,
          pre: values_to_string(pre_parts), build: values_to_string(build_parts)}
        {:ok, v}
      # :( why
      :error ->
        :error
    end
  end

  def translate("v"<>string, opts) do
    translate(string, opts)
  end

  def translate(string, opts) do
    string
    |> String.replace("_", "-")
    |> translate_double_build(opts)
    |> translate_pre_splits(opts)
    |> translate_eugen(opts)
    |> translate_vsn_proper_numbers(opts)
    |> String.trim()
    |> String.trim("+")
    |> String.trim("-")
  end

  # +build+anotherbuild => +build.anotherbuild
  def translate_double_build(string, _opts) do
    case String.split(string, "+") do
      [_, _] -> string
      [_] -> string
      [rest | builds] ->
        rest <> prefix(join(builds, "."), "+")
    end
  end

  # The intent is to "fix" usage of "-" as a separator of pre-releases (instead of ".")
  # If there's some "." in the pre string, abort fix
  def translate_pre_splits(string, _opts) do
    [str | builds] = String.split(string, "+", parts: 2)
    case String.split(str, "-", parts: 2) do
      [_] -> string
      [vsn, pre_s] ->
        if String.contains?(pre_s, ".") do
          string
        else
          pre_s = String.replace(pre_s, "-", ".")
          build_s = if builds, do: "+#{builds}", else: ""
          vsn <> "-" <> pre_s <> build_s
        end
    end
  end

  # who puts the "rc" directly after major.minor.patch without a separator ? :(
  def translate_eugen(string, _opts) do
    # split on everything that can be after the x.y.z
    {str, pre_and_builds} = case String.split(string, ~r/[-\+]/, parts: 2, include_captures: true) do
      [str | pre_and_builds] -> {str, join(pre_and_builds)}
      [str] -> {str, ""}
    end
    case String.split(str, ~r/[^0-9.]+/, parts: 2, include_captures: true) do
      [vsn_str, c, pre] ->
        pre = c <> pre
        [pres | builds] = String.split(pre_and_builds, ~r/\+/, parts: 2, include_captures: true)

        pres = join([pre, unprefix(pres, "-")], ".")
        vsn_str <> "-" <> pres <> join(builds)
      [_] -> string
    end
  end

  defp translate_vsn_proper_numbers(string, _opts) do
    [vsn, pre_and_builds] = case String.split(string, ~r/[-\+]/, parts: 2, include_captures: true) do
      [str | pre_and_builds] -> [str, join(pre_and_builds)]
      [str] -> [str, ""]
    end

    case Version.parse(vsn) do
      {:ok, _} -> string
      :error ->
        nums = String.split(vsn, ".")
        |> Enum.map(fn(num_s) ->
          case Integer.parse(num_s) do
            {num, _rest} -> Kernel.to_string(num)
            _ -> nil
          end
        end)
        |> Enum.filter(fn(x) -> x end)
        |> Enum.join(".")

        nums <> pre_and_builds
    end
  end



  def unprefix("-"<>string, "-"), do: string
  def unprefix("+"<>string, "+"), do: string
  def unprefix(string, _), do: string

  def prefix("", _), do: ""
  def prefix(nil, _), do: ""
  def prefix(string, prefix), do: prefix <> string

  def join(collection), do: join(collection, "")
  def join(collection, with) when is_list(collection) do
    collection
    |> List.flatten()
    |> Enum.filter(fn(item) ->
      item && item != ""
    end)
    |> Enum.join(with)
  end

  def join(_, _) do
    ""
  end

  def values_to_string(list) do
    Enum.map(list, fn(item) -> Kernel.to_string(item) end)
  end

  # Largely taken from Elixir's own Version.Parser, but way way more laxist
  defmodule Parser do

  end

  defimpl String.Chars, for: Fd.Version do
    def to_string(version) do
      pre = append(version.pre, "-")
      build = append(version.build, "+")
      vsn = [version.major, version.minor, version.patch]
      |> Enum.filter(fn(v) -> v end)
      |> Enum.join(".")

      [vsn, pre, build]
      |> Enum.join("")
    end

    defp append(nil, _), do: ""
    defp append([], _), do: ""

    defp append(pre, sign) do
      sign <>
        Enum.map_join(pre, ".", fn
          int when is_integer(int) -> Integer.to_string(int)
          string when is_binary(string) -> string
        end)
    end
  end

end
