defmodule FdWeb.Tools.NodeinfoController do
  use FdWeb, :controller

  def index(conn, _) do
    conn
    |> assign(:title, "Nodeinfo Validator")
    |> render("index.html")
  end

  def post(conn, %{"test" => %{"domain" => hostname}}) do
    conn
    |> redirect(to: nodeinfo_tool_path(conn, :show, hostname))
  end

  def show(conn, %{"hostname" => hostname}) do
    crawler = %Fd.Instances.Crawler{instance: %Fd.Instances.Instance{domain: hostname}}
    |> Fd.Instances.Crawler.Nodeinfo.query_well_known(always_halt: true, get_all: true)

    conn
    |> assign(:title, "Nodeinfo Validator: #{hostname}")
    |> render("show.html", hostname: hostname, crawler: crawler)
  end

end
