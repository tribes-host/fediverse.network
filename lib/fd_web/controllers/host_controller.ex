defmodule FdWeb.HostController do
  use FdWeb, :controller

  alias Fd.Repo
  alias Fd.Instances
  alias Fd.Instances.Instance
  import Ecto.Query, warn: false

  def index(conn, _params) do
    stats = Fd.GlobalStats.get()
    isps = from(i in Instance,
      select: %{count: fragment("count(id) as count"),
        users: fragment("sum(users) as users"),
        statuses: fragment("sum(statuses) as statuses"),
        isp: fragment("unnest(isps) as isp")
      },
      where: not is_nil(i.server) and i.server > 0 and i.up == true,
      group_by: fragment("isp"),
      order_by: fragment("count desc")
    )
    |> Repo.all

    conn
    |> assign(:title, "Hosting")
    |> put_default_cache()
    |> render("index.html", stats: stats, isps: isps)
  end

end
