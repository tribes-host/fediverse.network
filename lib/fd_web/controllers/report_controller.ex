defmodule FdWeb.ReportController do
  use FdWeb, :controller

  alias Fd.Stats

  @reports %{
    "2018" => {"🎉 2018 Report", %{"from" => "2018-04-30", "to" => "2018-12-31"}},
    "2019-01" => {"January 2019 Report", %{"from" => "2019-01-01", "to" => "2019-01-31"}},
    "2019-02" => {"February 2019 Report", %{"from" => "2019-02-01", "to" => "2019-02-28"}},
    "2019-03" => {"March 2019 Report", %{"from" => "2019-03-01", "to" => "2019-03-31"}},
    "2019-04" => {"April 2019 Report", %{"from" => "2019-04-01", "to" => "2019-04-30"}},
    "2019-05" => {"May 2019 Report", %{"from" => "2019-05-01", "to" => "2019-05-31"}},
  }

  def index(conn, _) do
    conn
    |> assign(:title, "Reports")
    |> render("index.html", reports: Enum.reverse(@reports))
  end

  for {key, {title, options}} <- @reports do
    def show(conn, %{"report" => unquote(key)}) do
      stats = Stats.file_evolution(unquote(Macro.escape(options)))

      conn
      |> assign(:title, unquote(title))
      |> render("show.html", [
        stats: stats,
        name: unquote(key)
      ])
    end
  end

end
