defmodule FdWeb.Api.GlobalStatsController do
  use FdWeb, :controller

  def index(conn, _) do
    stats = Fd.GlobalStats.get()
    |> Map.drop(["per_server", "countries", "isps"])

    json(conn, stats)
  end

  def isps(conn, _) do
    stats = Fd.GlobalStats.get()
    |> Map.get("isps")

    json(conn, stats)
  end

  def countries(conn, _) do
    stats = Fd.GlobalStats.get()
    |> Map.get("countries")

    json(conn, stats)
  end


end
