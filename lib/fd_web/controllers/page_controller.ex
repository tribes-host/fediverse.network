defmodule FdWeb.PageController do
  use FdWeb, :controller

  @pages %{
    "/about" => %{
      file: "about.html",
      title: "About"
    },
    "/about/faq" => %{
      file: "faq.html",
      title: "Frequently Asked Questions"
    },
    "/about/details" => %{
      file: "details.html",
      title: "Technical details"
    },
    "/monitoring" => %{
      file: "monitoring.html",
      title: "Monitoring service"
    },
    "/api" => %{
      file: "api.html",
      title: "API"
    },
    "/api/charts" => %{
      file: "charts.html",
      title: "Charts API"
    },
  }
  @paths Map.keys(@pages)

  @redirects %{
    "/info" => "/about/details",
    "/about/more" => "/about",
    "/about/charts" => "/api/charts"
  }

  def paths, do: @paths

  alias Fd.Instances

  for {from, to} <- @redirects do
    def show(conn = %{request_path: unquote(from)}, _) do
      conn
      |> redirect(to: unquote(to))
    end
  end

  for {path, params} <- @pages do
    file = Map.get(params, :file)
    title = Map.get(params, :title)
    def show(conn = %{request_path: unquote(path)}, _) do
      conn
      |> put_resp_header("cache-control", "public, max-age=600, stale-while-revalidate=3600, stale-if-error=3600")
      |> assign(:title, unquote(title))
      |> assign(:body_class, "page")
      |> render(unquote(file))
    end
  end

  def stats(conn, params) do
    stats = get_global_stats(params)
    render(conn, "stats.html", stats: stats)
  end

  def trap(conn, _) do
    conn
    |> redirect(external: "http://ping.online.net/10000Mo.dat")
  end

  def not_found(conn, _) do
    conn
    |> send_resp(404, "Not found")
  end


  defp get_global_stats(params) do
    interval      = Map.get(params, "interval", "3hour")
    Fd.Cache.lazy("global_stats:#{interval}", fn(interval) ->
      stats         = Instances.get_global_statistics(interval)
      get_serie = fn(stats, key) ->
        Enum.map(stats, fn(stat) -> Map.get(stat, key, 0)||0 end)
        |> Enum.reverse()
      end
      get_mg_serie = fn(stats, key) ->
        stats
        |> Enum.map(fn(stat) ->
          value = Map.get(stat, key, 0)||0
          date = Map.get(stat, "date")
          %{"date" => date, "value" => value}
        end)
        |> Enum.reverse
      end
      %{
        dates: get_serie.(stats, "date"),
        users: get_serie.(stats, "users"),
        statuses: get_serie.(stats, "statuses"),
        peers: get_serie.(stats, "peers"),
        emojis: get_serie.(stats, "emojis"),
        mg_users: get_mg_serie.(stats, "users"),
        mg_new_users: get_mg_serie.(stats, "new_users"),
        mg_statuses: get_mg_serie.(stats, "statuses"),
        mg_new_statuses: get_mg_serie.(stats, "new_statuses"),
        mg_peers: get_mg_serie.(stats, "peers"),
        mg_new_peers: get_mg_serie.(stats, "new_peers"),
        mg_emojis: get_mg_serie.(stats, "emojis"),
        mg_new_emojis: get_mg_serie.(stats, "new_emojis"),
        mg_instances: get_mg_serie.(stats, "instances"),
        interval: interval,
    } end, [interval])
  end

end
