defmodule FdWeb.ServerController do
  use FdWeb, :controller

  def index(conn, _params) do
    stats = Fd.GlobalStats.get()
    servers = Fd.ServerName.list()
    |> Enum.filter(fn(server) -> !server.hidden && server.id > 0 end)
    |> Enum.sort_by(fn(server) ->
      get_in(@stats, ["per_server", server.id, "instances", "up"])||0
    end)
    |> Enum.with_index()

    conn
    |> assign(:title, "Servers")
    |> put_default_cache()
    |> render("index.html", stats: stats, servers: servers)
  end

end
