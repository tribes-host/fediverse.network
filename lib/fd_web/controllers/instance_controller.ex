defmodule FdWeb.InstanceController do
  use FdWeb, :controller

  alias Fd.Instances
  alias Fd.Repo
  alias Fd.Instances.{Instance, InstanceCheck}
  import Ecto.Query, warn: false

  plug :cache_headers

  def cache_headers(conn, _) do
    conn
  end

  def tags(conn, params) do
    tags = from(t in Fd.Tags.Tag)
    |> join(:left, [t], tg in Fd.Tags.Tagging, on: ((tg.tag_id == t.id) or (tg.tag_id in t.includes)))
    |> where([t, tagging], is_nil(t.canonical_id) and is_nil(t.includes))
    |> select([t, tagging], {t, tagging})
    |> Repo.all()

    conn
    |> assign(:title, "Topics")
    |> put_list_cache()
    |> render("tags.html", tags: tags)
  end

  def index(conn, %{"tag" => tag_name} = params) do
    {_, filters, stats} = basic_filter(conn, Map.put(params, "up", "all"))

    tag = from(t in Fd.Tags.Tag, where: t.name == ^tag_name)
        |> Repo.one!

    tag = if tag.canonical_id do
      Repo.preload(tag, :canonical).canonical
    else
      tag
    end

    if tag_name == tag.name do
      i = from(t in Fd.Tags.Tag, where: t.id == ^tag.id or (t.canonical_id == ^tag.id))
      |> join(:inner, [t], tg in Fd.Tags.Tagging, on: (tg.tag_id == t.id) or (tg.tag_id in t.includes))
      |> join(:inner, [t, tg], i in Fd.Instances.Instance, on: tg.instance_id == i.id)
      |> select([t, _, i], i)
      |> Repo.all

      ids = Enum.map(i, fn(i) -> i.id end)
      stats = from(i in Instance,
        where: i.id in ^ids,
        select: %{
          "instances" => count(i.id),
          "users" => sum(i.users),
          "statuses" => sum(i.statuses)
        })
        |> Repo.one

      title = "#{tag.name} instances"

      conn
      |> assign(:title, title)
      |> put_list_cache()
      |> assign(:tag, tag)
      |> render("index.html", instances: i, title: title, stats: stats, filters: nil)
    else
      redirect(conn, to: instance_tag_path(conn, :index, tag.name))
    end
  end

  def index(conn = %{request_path: "/all"}, params) do
    {instances, filters, stats} = basic_filter(conn, Map.put(params, "up", "all"))
    conn
    |> assign(:title, "All Instances")
    |> put_list_cache()
    |> render("index.html", stats: stats, instances: instances, title: "All Instances", filters: filters)
  end

  def index(conn = %{request_path: "/down"}, params) do
    {instances, filters, stats} = basic_filter(conn, Map.put(params, "up", "false"))
    conn
    |> assign(:title, "Down Instances")
    |> put_list_cache()
    |> render("index.html", stats: stats, instances: instances, title: "Down Instances", filters: filters)
  end

  def index(conn = %{request_path: "/oldest"}, params) do
    {instances, filters, stats} = basic_filter(conn, Map.put(params, "age", "oldest"))
    conn
    |> assign(:title, "Oldest Instances")
    |> put_list_cache()
    |> render("index.html", stats: stats, instances: instances, title: "Oldest Instances", filters: filters)
  end

  def index(conn = %{request_path: "/newest"}, params) do
    {instances, filters, stats} = basic_filter(conn, Map.put(params, "age", "newest"))
    conn
    |> assign(:title, "Newest Instances")
    |> put_list_cache()
    |> render("index.html", stats: stats, instances: instances, title: "Newest Instances", filters: filters)
  end

  def index(conn = %{request_path: "/closed"}, params) do
    {instances, filters, stats} = basic_filter(conn, Map.merge(params, %{"up" => "all", "closed" => "true"}))
    conn
    |> assign(:title, "Closed Instances")
    |> put_list_cache()
    |> render("index.html", stats: stats, instances: instances, title: "Closed Instances", filters: filters)
  end

  def tld(conn, _) do
    stats = Fd.HostStats.get()
    conn
    |> assign(:title, "Instances per TLD")
    |> render("tld.html", stats: stats)
  end

  def domain(conn, _) do
    stats = Fd.HostStats.get()
    conn
    |> assign(:title, "Instances per domain")
    |> render("domain.html", stats: stats)
  end


  for s <- Fd.ServerName.list_names() do
    path = Fd.ServerName.route_path(s)
    display_name = Fd.ServerName.display_name(s)
    def index(conn = %{request_path: unquote(path)}, params) do
      params = params
      |> Map.put("server", unquote(path))
      |> put_default_params()
      {instances, filters, stats} = basic_filter(conn, params)
      conn
      |> assign(:title, unquote(display_name))
      |> put_list_cache()
      |> render("index.html", stats: stats, instances: instances, title: "#{unquote(display_name)} Instances", filters: filters)
    end
  end

  for skip <- ["activity", "count", "signup", "statuses", "users", "peers", "domain", "tld", "tag", "age"] do
    defp put_default_params(params = %{unquote(skip) => _}), do: params
  end
  defp put_default_params(params) do
    params
    |> Map.put("activity", "score")
    |> Map.put("period", "1h")
    |> Map.put("sort", "desc")
  end

  def index(conn, params) do
    params = put_default_params(params)
    {instances, filters, stats} = basic_filter(conn, params)
    title = case params do
      %{"domain" => domain} -> "Instances on #{domain} sub-domains"
      %{"tld" => tld} -> "Instances on TLD .#{tld}"
      _ -> "Instances"
    end
    conn
    |> assign(:title, title)
    |> put_list_cache()
    |> render("index.html", stats: stats, instances: instances, title: title, filters: filters)
  end

  def new(conn, _params) do
    changeset = Instances.change_instance(%Instance{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"instance" => instance_params}) do
    case Instances.create_instance(instance_params) do
      {:ok, instance} ->
        conn
        |> put_flash(:info, "Instance added successfully.")
        |> redirect(to: instance_path(conn, :show, instance))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def federation(conn, params = %{"instance_id" => id}) do
    instance = Instances.get_instance_by_domain!(id)
    restrictions = Instances.restrictions(instance)
    restricted = if Map.get(instance.settings || %{}, :quarantine_restrictions, false) do
      []
    else
      Instances.restricted(instance)
    end
    conn
    |> assign(:title, "#{Fd.Util.idna(instance.domain)} Federation Restrictions")
    |> assign(:private, Instances.Instance.hidden?(instance))
    |> assign(:section, "federation")
    |> render(FdWeb.InstanceFederationView, "show.html", instance: instance, restrictions: restrictions, restricted: restricted)
  end

  def score(conn, params = %{"instance_id" => id}) do
    instance = Instances.get_instance_by_domain!(id)
    conn
    |> assign(:title, "#{Fd.Util.idna(instance.domain)} Scores")
    |> assign(:private, Instances.Instance.hidden?(instance))
    |> render("score.html", instance: instance)
  end

  def public_timeline(conn, params = %{"instance_id" => id}) do
    instance = Instances.get_instance_by_domain!(id)
    if instance.server == 2 or instance.server == 3 do

      timeline_url = Pleroma.Web.MediaProxy.url("https://#{instance.domain}/api/v1/timelines/public.json?limit=50&local=true")

      req = timeline_url
             |> HTTPoison.get!()

      data = Poison.decode!(req.body)

      conn
      |> json(data)
    else
      send_resp(conn, 404, "not supported")
    end
  end

  def timeline(conn, params = %{"instance_id" => id}) do
    instance = Instances.get_instance_by_domain!(id)
    if instance.server == 2 or instance.server == 3 do

      conn
      |> assign(:title, "#{Fd.Util.idna(instance.domain)} Timeline")
      |> assign(:private, Instances.Instance.hidden?(instance))
      |> assign(:section, "timeline")
      |> render("timeline.html", instance: instance)
    else
      send_resp(conn, 404, "not supported")
    end
  end

  def show(conn, params = %{"id" => id}) do
    instance      = Instances.get_instance_by_domain!(id)
    iid           = instance.id
    checks        = Fd.Instances.get_instance_last_checks_overview(instance, 220)
    last_up_check = Instances.get_instance_last_up_check(instance)
    host_stats    = Fd.HostStats.get()
    stats         = Fd.Cache.lazy("web:i:#{instance.id}:stats", &get_instance_stats/2, [instance, Map.put(params,
      "neg_as_zero", true)])

    if Application.get_env(:fd, :instances)[:readrepair] do
      Fd.Instances.Server.crawl(instance.id)
    end

    conn
    |> assign(:title, "#{Fd.Util.idna(instance.domain)} - #{Fd.ServerName.from_int(instance.server)}")
    |> assign(:private, Instances.Instance.hidden?(instance))
    |> assign(:section, "summary")
    |> render("show.html", instance: instance, last_up_check: last_up_check, checks: checks, host_stats: host_stats, stats: stats)
  end

  def stats(conn, params = %{"instance_id" => id}) do
    instance      = Instances.get_instance_by_domain!(id)
    stats         = Fd.Cache.lazy("web:i:#{instance.id}:stats:#{conn.query_string}", &get_instance_stats/2, [instance, params])

    conn
    |> assign(:title, "#{Fd.Util.idna(instance.domain)} statistics")
    |> assign(:section, "stats")
    |> assign(:private, Instances.Instance.hidden?(instance))
    |> render("stats.html", instance: instance, stats: stats)
  end

  def checks(conn, params = %{"instance_id" => id}) do
    instance      = Instances.get_instance_by_domain!(id)
    checks        = Instances.get_instance_last_checks(instance, 500)

    conn
    |> assign(:title, "#{Fd.Util.idna(instance.domain)} checks")
    |> assign(:section, "checks")
    |> assign(:private, Instances.Instance.hidden?(instance))
    |> render("checks.html", instance: instance, checks: checks)
  end

  def nodeinfo(conn, params = %{"instance_id" => id}) do
    instance = Instances.get_instance_by_domain!(id)

    if instance.nodeinfo && !Enum.empty?(instance.nodeinfo) do
      conn
      |> assign(:title, "#{Fd.Util.idna(instance.domain)} nodeinfo")
      |> assign(:private, Instances.Instance.hidden?(instance))
      |> render("nodeinfo.html", instance: instance)
    else
      conn
      |> redirect(to: instance_path(conn, :show, instance))
    end
  end

  @allowed_filters ["up", "closed", "server", "age", "tld", "domain", "users", "statuses", "emojis", "peers",
    "max_chars",
    "country", "isp", "ips", "features", "protocols",
    "build",
    "registrations",
    "count", "activity", "score", "period", "sort"] # (period & on are args for active)
  defp basic_filter(conn,params) do
    Fd.Cache.lazy("list:#{conn.request_path}?#{conn.query_string}", &run_basic_filter/1, [params])
  end
  defp run_basic_filter(params) do
    filters = params
    |> Enum.map(fn({param, value}) ->
      value = case value do
        "/"<>something -> something
        other -> other
      end
      {param, value}
    end)
    |> Enum.filter(fn({key, _}) -> Enum.member?(@allowed_filters, key) end)
    |> Enum.into(Map.new)
    |> Map.put_new("up", "true")
    |> Map.put_new("server", "known")

    filters = if count_by = Map.get(filters, "count") do
      sort = Map.get(filters, "sort", "desc")
      filters
      |> Map.put(count_by, sort)
    else
      filters
    end
    filters = if Map.get(filters, "age") == "created" do
      sort = case Map.get(filters, "sort", "desc") do
        "asc" -> "oldest"
        _ -> "newest"
      end
      filters
      |> Map.put("age", sort)
    else
      filters
    end


    instances = Enum.reduce(filters, from(i in Instance), &basic_filter_reduce/2)
    |> select([q], %Instance{id: q.id, domain: q.domain, up: q.up, server: q.server, statuses: q.statuses, users: q.users,
      peers: q.peers, emojis: q.emojis, hidden: q.hidden, signup: q.signup, dead: q.dead, version: q.version,
      inserted_at: q.inserted_at, max_chars: q.max_chars, settings: q.settings, nodeinfo: q.nodeinfo, semver: q.semver,
      countries: q.countries, isps: q.isps})
    |> Fd.Repo.all
    |> Fd.Repo.preload(:tags)
    |> reduce_basic_filter(filters)

    {instances, filters, Fd.GlobalStats.get()}
  end

  defp reduce_basic_filter(instances, params = %{"activity" => activity}) do
    sorter = case Map.get(params, "sort") do
      "asc" -> &>=/2
      _ -> &<=/2
    end
    period = case Map.get(params, "period") do
      p when p in ["1h", "6h", "12h", "24h", "1w"] -> p
      _ -> "1h"
    end
    activity = case activity do
      "users" -> :user_rank
      "score" -> :activity_rank
      _ -> :status_rank
    end
    instances
    |> Enum.map(fn(instance) -> {Fd.Instances.Score.activity(instance, activity, period), instance} end)
    |> Enum.filter(fn({score, _}) -> is_integer(score) && score != 0 end)
    |> Enum.sort_by(fn({score, _}) -> score end, sorter)
    |> Enum.map(fn({_, instance}) -> instance end)
  end

  defp reduce_basic_filter(instances, %{"score" => sort}) do
    sorter = case sort do
      "desc" -> &<=/2
      _ -> &>=/2
    end
    Enum.sort_by(instances, fn(instance) -> Fd.Instances.Score.score(instance) end, sorter)
  end

  defp reduce_basic_filter(instances, _), do: instances

  def get_instance_stats(instance, params) do
    intervals     = ["monthly", "weekly", "daily", "hourly", if(instance.monitor, do: "10m")]
    |> Enum.filter(fn(x) -> x end)
    periods = ["24 hours", "48 hours", "7 days", "14 days", "31 days",
    "90 days", "182 days"]#, "365 days", "720 days"]
    interval      = Map.get(params, "interval", "hourly")
    period        = Map.get(params, "period", nil)
    non_neg       = if Map.get(params, "neg_as_zero", false), do: true, else: false
    {stats, ttl, period}         = Instances.get_instance_statistics(instance.id, interval, period)
    get_serie = fn(stats, key) ->
      Enum.map(stats, fn(stat) -> Map.get(stat, key, 0)||0 end)
      |> Enum.reverse()
    end
    get_mg_serie = fn(stats, key, opts) ->
      stats
      |> Enum.map(fn(stat) ->
        value = Map.get(stat, key)
        value = if Keyword.get(opts, :non_neg, false) && value < 0 do
          0
        else value end
        date = Map.get(stat, "date")
        %{"date" => date, "value" => value}
      end)
      |> Enum.reverse
    end
    stats = %{
      #dates: get_serie.(stats, "date"),
      #users: get_serie.(stats, "users"),
      #statuses: get_serie.(stats, "statuses"),
      #peers: get_serie.(stats, "peers"),
      #emojis: get_serie.(stats, "emojis"),
      mg_users: get_mg_serie.(stats, "users", []),
      mg_new_users: get_mg_serie.(stats, "new_users", [non_neg: non_neg]),
      mg_statuses: get_mg_serie.(stats, "statuses", []),
      mg_new_statuses: get_mg_serie.(stats, "new_statuses", [non_neg: non_neg]),
      mg_peers: get_mg_serie.(stats, "peers", []),
      mg_new_peers: get_mg_serie.(stats, "new_peers", [non_neg: non_neg]),
      mg_emojis: get_mg_serie.(stats, "emojis", []),
      mg_new_emojis: get_mg_serie.(stats, "new_emojis", [non_neg: non_neg]),
      interval: interval,
      intervals: intervals,
      period: period,
      periods: periods,
      neg_as_zero: non_neg
    }
    {:ok, stats, ttl}
  end

  defp basic_filter_reduce({"up", "true"}, query) do
    where(query, [i], i.up == true)
  end
  defp basic_filter_reduce({"up", "false"}, query) do
    where(query, [i], i.up != true)
  end
  defp basic_filter_reduce({"up", "all"}, query) do
    query
  end

  defp basic_filter_reduce({"closed", "true"}, query) do
    where(query, [i], i.dead or fragment("? < NOW() - INTERVAL '30 days'", i.last_up_at))
    |> order_by([i], [desc: i.last_up_at])
  end

  defp basic_filter_reduce({"server", "all"}, query) do
    query
  end
  defp basic_filter_reduce({"server", "known"}, query) do
    where(query, [i], i.server != 0)
  end
  defp basic_filter_reduce({"server", server}, query) do
    int = Fd.ServerName.to_int(String.downcase(server))
    where(query, [i], i.server == ^int)
  end

  defp basic_filter_reduce({"country", country}, query) do
    country = String.upcase(country)
    where(query, [i], fragment("? @> ARRAY[?]", i.countries, ^country))
  end

  defp basic_filter_reduce({"protocols", protocols}, query) do
    protocols = String.split(protocols, ",")
    |> Enum.map(fn(x) -> String.downcase(x) end)
    where(query, [i], fragment("? @> ARRAY[?]", i.protocols, ^protocols))
  end

  defp basic_filter_reduce({"features", features}, query) do
    features = String.split(features, ",")
    |> Enum.map(fn(x) -> String.downcase(x) end)
    where(query, [i], fragment("? @> ?", i.features, ^features))
  end

  defp basic_filter_reduce({"ips", ips}, query) do
    ips = String.split(ips, ",")
    |> Enum.map(fn(x) -> String.downcase(x) end)
    where(query, [i], fragment("? @> ?", i.ips, ^ips))
  end

  defp basic_filter_reduce({"isp", isp}, query) do
    where(query, [i], fragment("? @> ?", i.isps, ^isp))
  end

  defp basic_filter_reduce({"build", build}, query) do
    String.split(build, ",")
    where(query, [i], fragment("?->'build' @> ?", i.semver, ^build))
  end

  defp basic_filter_reduce({"registrations", "true"}, query) do
    where(query, [i], i.signup == true)
  end

  defp basic_filter_reduce({"registrations", "false"}, query) do
    where(query, [i], i.signup == false)
  end

  defp basic_filter_reduce({"registrations", "unknown"}, query) do
    where(query, [i], is_nil(i.signup))
  end


  defp basic_filter_reduce({"age", "oldest"}, query) do
    order_by(query, [i], [asc: i.inserted_at])
  end
  defp basic_filter_reduce({"age", "newest"}, query) do
    order_by(query, [i], [desc: i.inserted_at])
  end

  defp basic_filter_reduce({"users", "asc"}, query) do
    query
    |> where([i], not is_nil(i.users))
    |> order_by([i], [asc: i.users])
  end
  defp basic_filter_reduce({"users", "desc"}, query) do
    query
    |> where([i], not is_nil(i.users))
    |> order_by([i], [desc: i.users])
  end

  defp basic_filter_reduce({"statuses", "asc"}, query) do
    query
    |> where([i], not is_nil(i.statuses))
    |> order_by([i], [asc: i.statuses])
  end
  defp basic_filter_reduce({"statuses", "desc"}, query) do
    query
    |> where([i], not is_nil(i.statuses))
    |> order_by([i], [desc: i.statuses])
  end

  defp basic_filter_reduce({"max_chars", "asc"}, query) do
    query
    |> where([i], not is_nil(i.max_chars))
    |> order_by([i], [asc: i.max_chars])
  end
  defp basic_filter_reduce({"max_chars", "desc"}, query) do
    query
    |> where([i], not is_nil(i.max_chars))
    |> order_by([i], [desc: i.max_chars])
  end

  defp basic_filter_reduce({"emojis", "asc"}, query) do
    query
    |> where([i], not is_nil(i.emojis))
    |> order_by([i], [asc: i.emojis])
  end
  defp basic_filter_reduce({"emojis", "desc"}, query) do
    query
    |> where([i], not is_nil(i.emojis))
    |> order_by([i], [desc: i.emojis])
  end

  defp basic_filter_reduce({"peers", "asc"}, query) do
    query
    |> where([i], not is_nil(i.peers))
    |> order_by([i], [asc: i.peers])
  end
  defp basic_filter_reduce({"peers", "desc"}, query) do
    query
    |> where([i], not is_nil(i.peers))
    |> order_by([i], [desc: i.peers])
  end

  defp basic_filter_reduce({"tld", tld}, query) do
    tld = Fd.Util.from_idna(tld)
    where(query, [i], i.domain_suffix == ^tld)
  end
  defp basic_filter_reduce({"domain", domain}, query) do
    domain = Fd.Util.from_idna(domain)
    where(query, [i], i.domain_base == ^domain or i.domain == ^domain)
  end

  defp basic_filter_reduce(_, query), do: query

  defp put_list_cache(conn) do
    conn
    |> put_resp_header("cache-control", "public, max-age=300, stale-while-revalidate=600, stale-if-error=3600")
  end

end
