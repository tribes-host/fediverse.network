defmodule FdWeb.RestrictionRuleView do
  use FdWeb, :view

  @aliases %{
    "media_nsfw" => %{
      inbound: "Marks media as sensitive from",
      outbound: "Media marked as sensitive by"
    },
    "federated_timeline_removal" => %{
      inbound: "Removes messages on federated timeline from",
      outbound: "Removed from federated timeline by"
    },
    "reject" => %{
      inbound: "Rejects from",
      outbound: "Rejected by"
    },
    "media_removal" => %{
      inbound: "Removes media from",
      outbound: "Media removed by"
    },
    "accept" => %{
      inbound: "Only accepts from",
      outbound: "Accepted by"
    },
    "quarantined" => %{
      inbound: "Quarantines",
      outbound: "Quarantined by"
    },
    "banner_removal" => %{
      inbound: "Removes banners from",
      outbound: "Banners removed by"
    },
    "avatar_removal" => %{
      inbound: "Removes avatars from",
      outbound: "Avatars removed by",
    },
    "reject_report" => %{
      inbound: "Rejects reports from",
      outbound: "Reports rejected by",
    },
    "silence" => %{
      inbound: "Silences from",
      outbound: "Silenced by",
    }
  }
  @aliases_keys Map.keys(@aliases)

  @ways [:inbound, :outbound]
  def name(way, name) when way in @ways and name in @aliases_keys do
    get_in(@aliases, [name, way])
  end

  def name(way, name) do
    "#{name} #{to_string(way)}"
  end

end
