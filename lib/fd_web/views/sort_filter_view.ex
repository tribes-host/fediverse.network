defmodule FdWeb.SortFilterView do
  use FdWeb, :view

  alias FdWeb.Filters

  def select_sort(filters) do
    type = {current_type, type_val} = Filters.current_sort_type(filters) || {"unsorted", ""}

    unless current_type == "unsorted" do
      available = List.delete(Filters.sort_type_list, type)
      items = for {type,val} <- available do
        {Filters.type_title(type, val), "?#{type}=#{val}"}
      end

      [
        "Sorted by",
        raw("&nbsp;"),
        dropdown(Filters.type_title(current_type, type_val), items, button_class: "bold"),
        raw("&nbsp;"),
        select_dimensions(filters)
      ]
    else
      ""
    end
  end

  def select_dimensions(filters) do
    type = {current_type, type_val} = Filters.current_sort_type(filters) || {"unsorted", ""}
    for dimension_name <- Filters.dimensions(current_type) do
      current = Map.get(filters, dimension_name, Filters.dimension_default(current_type, dimension_name))
      dimension = Filters.dimension(current_type, dimension_name)
      items = for {value, title} when value != current <- dimension do
        {title, build_url(filters, %{dimension_name => value})} # "?#{current_type}=#{type_val}&#{dimension_name}=#{value}"}
      end
      dropdown(Filters.dimension_name(current_type, dimension_name, current), items)
    end
  end

  defp dropdown(title, items, options \\ []) do
    button_class = Keyword.get(options, :button_class, "")
    lis = for {name, link} <- items, do: content_tag(:li, [link(name, to: link)])
    button = content_tag(:button, title, class: button_class)
    ul = content_tag(:ul, lis)
    label = content_tag(:label, [tag(:input, type: "checkbox"), ul])

    content_tag(:span, [button, label], class: "css-dropdown")
  end

  def build_url(filters, new_filters) do
    filters
    |> Map.take(["activity", "count", "sort", "period", "field"])
    |> Map.merge(new_filters)
    |> Enum.reduce("?", fn({key, value}, acc) -> acc <> "&" <> key <> "=" <> value end)
  end


end
