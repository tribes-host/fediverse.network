defmodule FdWeb.InstanceFederationView do
  use FdWeb, :view
  alias Fd.Instances.Instance

  def render("show.html", %{conn: conn, restrictions: restrictions, restricted: restricted, instance: instance = %Instance{server: 3, nodeinfo: %{"metadata" => %{"federation" => federation = %{"mrf_policies" => policies}}}}}) do
    restrictions = Enum.reduce(restrictions, %{}, fn(r, acc) ->
      rule = Map.get(acc, r.rule, [])
      Map.put(acc, r.rule, [r.peered_instance | rule])
    end)
    restricted = Enum.reduce(restricted, %{}, fn(r, acc) ->
      rule = Map.get(acc, r.rule, [])
      Map.put(acc, r.rule, [r.instance | rule])
    end)
    render("pleroma_mrf.html", conn: conn, instance: instance, federation: federation, policies: policies, restrictions: restrictions, restricted: restricted)
  end

  def render("show.html", %{conn: conn, restrictions: restrictions, restricted: restricted, instance: instance =
    %Instance{restrictions_disclosed: true}}) do
    restrictions = Enum.reduce(restrictions, %{}, fn(r, acc) ->
      rule = Map.get(acc, r.rule, [])
      Map.put(acc, r.rule, [r.peered_instance | rule])
    end)
    restricted = Enum.reduce(restricted, %{}, fn(r, acc) ->
      rule = Map.get(acc, r.rule, [])
      Map.put(acc, r.rule, [r.instance | rule])
    end)
    render("generic.html", conn: conn, instance: instance, restrictions: restrictions, restricted: restricted)
  end

  def render("show.html", %{conn: conn, instance: instance}) do
    render("none.html", conn: conn, instance: instance)
  end

  def overview(conn, instance = %Instance{restrictions_disclosed: true}) do
    overview_content(link("disclosed", to: instance_instance_path(conn, :federation, instance)))
  end

  def overview(conn, instance = %Instance{settings: %{federation_restrictions_link: link}}) when is_binary(link) do
    overview_content(link("disclosed on an external list", to: link))
  end

  def overview(conn, %{server: server}) when server in [2,3] do
    overview_content("unknown/not disclosed")
  end

  def overview(conn, _), do: ""

  defp overview_content(content) do
    content_tag(:li, [
      content_tag(:span, "Federation restrictions", class: "label"),
      raw("&nbsp;"),
      content_tag(:span, content, class: "value")
    ])
  end

end

