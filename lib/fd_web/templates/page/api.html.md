# API

still not done, sorry.

* [Charts](/api/charts)

## List Filters / Sorting

**Filters**:

* **server** known (default), all, mastodon, pleroma, ….
* **up** true (default), false, all
* **closed** (no default), true
* **tld** (no default). Any top-level domain
* **domain** (no default). use any registerable domain.
* **country** (no default). iso country code.
* **ips** (no default, comma separated).
* **isps** (no default, comma separated).
* **build** (no default).
* **protocols** (no default, comma separated).
* **features** (no default, comma separated).
* **registrations** (no default) true, false, unknown.

**Ordering** (default is activity score with an hour period):

* **age** newest, oldest
* **users** desc, asc
* **statuses** desc, asc
* **peers** desc, asc
* **emojis** desc, asc
* **max_chars** desc, asc
* **activity** (default, score): users, statuses, score.
  * _period_: 1h, 6h, 12h, 24h, 7d
  * _sort_: desc, asc

