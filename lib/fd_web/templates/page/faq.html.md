## Frequently asked questions

### Can you add my instance ?
{: #add}

Instances are automatically detected as soon as they start federating with other instances: you and we don't have anything to do! Federate and wait :-)

### I can't login to manage my instance
{: #login}

This only works for Mastodon and Pleroma APIs and the e-mail need to be the same as the one you've set up in your server settings.

### Can you check less often ?
{: #crawlless}

[Login to manage](/manage) your instance and set the "calm" setting to on. If your instance is not supported by the manage area, send an email.

### Can you remove my instance ?
{: #remove}

You can easily unlist your instance from any fediverse list (not only fediverse.network ones) by switching on some "private" setting. Most fediverse servers support this with the exception of Mastodon.

For Mastodon owners, [login to manage](/manage) your instance and you'll be able to set it as private from here, but doing so on the other third party lists is up to you.

Depending on your servers settings, you can also opt-out of statistics collection (count of users, status, …).

We won't _remove_ any instance as long as it is part of the fediverse.

#### Why you don't respect robots.txt
{: #robotstxt}

Because we feel like it is not relevant in our case. We are not a search engine, and not indexing any content. We are just collecting statistics provided by the instance software which is communicating with other fediverse instances.

