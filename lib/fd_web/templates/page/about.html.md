## about fediverse.network

**Fediverse.network's goal is to build a comprehensible list/map and statistics of the known oStatus/ActivityPub fediverse.**

As side-goals, it provides a [monitoring service](/monitoring) for instance owners.

Warning: **The site and its data is far from perfect yet!**

Other protocols than oStatus/ActivityPub may be added some day, for more details see [the info page](/info).

### Contact
{: #contact}

* By e-mail: [root@fediverse.network](mailto:root@fediverse.network)
* On the fediverse: [@href@pleroma.fr](https://pleroma.fr/users/href)
* IRC: irc.random.sh #fd (TLS mandatory, port 6697)

The crawlers IPs are:

* crawler-us-il-1.fediverse.network (`163.237.218.13`, `2001:4978:1000:1399::13`)
* makhnovtchina.random.sh (`195.154.157.6`)

### API and database access
{: #apidb}

[Charts API](/about/charts).

The rest of the API is now under work for real!

Read-only access to the fediverse.network underlying database can be provided for research purposes, either by traditional postgresql access or by using [metabase](https://metabase.fediverse.network). Send an email!

### Source
{: #source}

Licensed under the AGPL v3! Find it at [git.yt/random/fediverse-network](https://git.yt/random/fediverse-network) (randomly mirrored on [sr.ht](https://git.sr.ht/~href/fediverse-network) and [github.com](https://github.com/fediverse/network)).

Written in Elixir, uses PostgreSQL with TimescaleDB, and licensed under AGPL; hosted on the glorious FreeBSD.

## Third Party data sources
{: #datasources}

* **GeoLite2 _Country_ and _ASN_**, licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/), created by MaxMind, available from [maxmind.com](https://www.maxmind.com).
* **Public Suffix List**, [publicsuffix.org](https://publicsuffix.org/).
* **[chartd.co](https://chartd.co)** is used to generate the png/svg charts used in lists/charts api.
* **[joinmastodon.org](https://joinmastodon.org)** server list data is used to know if the instance adheres to the Mastodon Covenant, and to get instance topics.
* **[instances.social](https://instances.social)** server list data is used to get instance topics/….

### Credits
{: #credits}

* Developed by [contributors](https://git.yt/random/fediverse-network/graphs/master) ([@href](https://soc.ialis.me/@href), [@hecate](https://soc.ialis.me/@hecate), [@lerk](https://comm.network/@lerk)) and hosted by [@href](https://soc.ialis.me/@href)
* Contains an embedded [Pleroma](https://pleroma.social) instance
* Thanks to [@dashie](https://pleroma.otter.sh) for supporting some of href's rants, helping in testing, reporting various issues, and for the [nodeinfo validator](/tools/nodeinfo) idea and well-known schema.
* Thanks to [@Famine](https://soc.ialis.me/@Famine) for the logo
* Thanks to [@feld](https://bikeshed.party/feld) for helping hosting the main crawler
* Thanks to [@rin](https://patch.cx/rin) for building fun queries out the data on metabase
* special thanks to the whole fediverse too!

