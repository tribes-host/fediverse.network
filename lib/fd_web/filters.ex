defmodule FdWeb.Filters do

  @filter_keys ["status", "server", "tld", "domain", "isp", "ip", "country", "registrations"]
  @sort_keys ["activity", "count", "age"]

  @dimensions %{
    "activity" => ["sort", "period"],
    "count" => ["sort"],
    "age" => ["sort"]
  }

  def sort_type_list() do
    [{"activity", "users"}, {"activity", "statuses"}, {"activity", "score"}, {"count", "users"}, {"count", "statuses"},
      {"count", "peers"}, {"age", "created"}]
  end

  def dimensions(type) do
    Map.get(@dimensions, type)
  end

  def dimension("activity", "period") do
    [{"1h", "in the last hour"}, {"3h", "in the last 3 hours"}, {"6h", "in the last 6 hours"},
    {"12h", "in the last 12 hours"}, {"24h", "in the last 24 hours"}, {"7d", "in the last 7 days"}]
  end

  def dimension("activity", "sort") do
    [{"desc", "most active"}, {"asc", "less active"}]
  end

  def dimension("count", "sort") do
    [{"desc", "bigger"}, {"asc", "smaller"}]
  end

  def dimension("age", "sort") do
    [{"desc", "newest"}, {"asc", "older"}]
  end

  def dimension_default("activity", "period"), do: "1h"
  def dimension_default(_, "sort"), do: "desc"

  def dimension_name(t, d, v) do
    dimension(t,d)
    |> Enum.into(Map.new)
    |> Map.get(v)
  end

  def current_sort_type(filters) do
    filters
    |> Map.take(@sort_keys)
    |> Enum.filter(fn({_, v}) -> v end)
    |> List.first
  end

  def type_title("unsorted", _), do: "nothing"
  def type_title("activity", "users"), do: "new users"
  def type_title("activity", "statuses"), do: "new statuses"
  def type_title("activity", "score"), do: "activity"

  def type_title("count", "users"), do: "users"
  def type_title("count", "statuses"), do: "statuses"
  def type_title("count", "peers"), do: "peers"

  def type_title("age", _), do: "creation date"

end
