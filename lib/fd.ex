defmodule Fd do
  @moduledoc """
  Fd keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  @env Mix.env()
  def build_env(), do: @env

  @doc "Crawls the instance at `domain`."
  @spec crawl(String.t | integer) :: nil
  def crawl(domain) when is_binary(domain) do
    instance = Fd.Instances.get_instance_by_domain!(domain)
    Fd.Instances.Server.crawl(instance.id)
  end
  def crawl(id) when is_integer(id) do
    Fd.Instances.Server.crawl(id)
  end

  @samplecrawl [
    "pleroma.site",
    "pleroma.fr",
    "mastodon.social",
    "gab.com",
    "pleroma.otter.sh",
    "eldritch.cafe",
    "cybre.space",
    "radical.town",
    "social.myfreecams.com",
    "humblr.social",
    "friends.cafe",
    "witches.live",
    "write.as",
    "fediverse.blog",
  ]
  def samplecrawl() do
    for c <- @samplecrawl, do: crawl(c)
  end

  def user_agent(name \\ "crawler", extra \\ nil) do
    dev_ua = if @env == :dev, do: " [dev]", else: ""
    extras = if extra, do: " #{extra}", else: ""
    "fediverse.network #{name}#{dev_ua} (https://fediverse.network/about#{extras} root@fediverse.network)"
  end

end
