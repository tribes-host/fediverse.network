defmodule Fd.VersionTest do
  use Fd.DataCase

  # seriously people: use semver.

  @cases %{
    "1.0.0-develop-some_underscores" =>
    %{major: 1, minor: 0, patch: 0, pre: ["develop", "some", "underscores"]},
    "1.0.0" =>
      %{major: 1, minor: 0, patch: 0},
    "2.9.0rc1" =>
      %{major: 2, minor: 9, patch: 0, pre: ["rc1"]},
    "11.23" =>
      %{major: 11, minor: 23},
    "2018.08" =>
      %{major: 2018, minor: 8},
    "2018.08+dev" =>
      %{major: 2018, minor: 8, build: ["dev"]},
    "2018.08-rc1" =>
      %{major: 2018, minor: 8, pre: ["rc1"]},
    "2.7.0+glitch" =>
      %{major: 2, minor: 7, patch: 0, build: ["glitch"]},
    "2.7.0rc1+glitch+sleeping" =>
      %{major: 2, minor: 7, patch: 0, pre: ["rc1"], build: ["glitch", "sleeping"]},
    "2.9.2-build.7428+g5b20284.with.comm_cx" =>
      %{major: 2, minor: 9, patch: 2, pre: ["build", 7428], build: ["g5b20284", "with", "comm-cx"]},
    "0.9.0-1967-g33e3a7ba+fedinet" =>
      %{major: 0, minor: 9, patch: 0, pre: [1967, "g33e3a7ba"], build: ["fedinet"]},
    "1.0.0-21-g9b1e75ba-develop+dev" =>
      %{major: 1, minor: 0, patch: 0, pre: [21, "g9b1e75ba", "develop"], build: ["dev"]},
    "1.0.0-21-g9b1e75ba.develop+dev" =>
      %{major: 1, minor: 0, patch: 0, pre: ["21-g9b1e75ba", "develop"], build: ["dev"]},
    "1.0.0-21-g9b1e75ba-develop+dev" =>
      %{major: 1, minor: 0, patch: 0, pre: [21, "g9b1e75ba", "develop"], build: ["dev"]},
    "1.0.0-master+dev" =>
      %{major: 1, minor: 0, patch: 0, pre: ["master"], build: ["dev"]},
    "2.9.0+glitch+vulpine-prod-20190622-01" =>
      %{major: 2, minor: 9, patch: 0, build: ["glitch", "vulpine-prod-20190622-01"]},
  }

  describe "vsn" do
    for {string, asserts} <- @cases do

      test "#{inspect string}" do

        for key <- Map.keys(Map.from_struct(%Fd.Version{})) do
          string = unquote(string)
          asserts = struct(Fd.Version, unquote(Macro.escape(asserts)))
          assert {:ok, results} = Fd.Version.parse(unquote(string), [])
          assert Map.get(results, :__struct__) == Fd.Version
          asserted = case key do
            :original ->
              string
            # The cleaned version of the original string should match
            # the one returned by Fd.Version String.Char's implementation.
            :normalized ->
              String.Chars.to_string(results)
            _ ->
              Map.get(asserts, key, nil)
          end
            result = Map.get(results, key)
            assert {key, result} = {key, asserted}
        end

      end
    end
  end

  test "to_vsn" do
    assert {:ok, result} = Fd.Version.parse("30.2019.20rc1")
    assert "30.2019.20" = Fd.Version.to_vsn(result)
  end

  test "to_vsn_major_minor" do
    assert {:ok, result} = Fd.Version.parse("2.8.3")
    assert "2.8" = Fd.Version.to_vsn_major_minor(result)
  end

end
