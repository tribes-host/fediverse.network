defmodule Fd.Repo.Migrations.InstanceRestrictionRules do
  use Ecto.Migration

  def change do
    create table(:instance_restriction_rules, primary_key: false) do
      add :instance_id, references(:instances, on_delete: :delete_all)
      add :peered_instance_id, references(:instances, on_delete: :delete_all)
      add :rule, :string
      timestamps(updated_at: false)
    end
    create unique_index(:instance_restriction_rules, [:instance_id, :peered_instance_id, :rule])
    create index(:instance_restriction_rules, [:instance_id, :rule])
    create index(:instance_restriction_rules, [:peered_instance_id, :rule])
    create index(:instance_restriction_rules, [:instance_id])
    create index(:instance_restriction_rules, [:peered_instance_id])
    create index(:instance_restriction_rules, [:rule])
  end

end
