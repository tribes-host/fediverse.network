defmodule Fd.Repo.Migrations.InstanceRestrictionsDisclosed do
  use Ecto.Migration

  def change do
    alter table(:instances) do
      add(:restrictions_disclosed, :boolean)
    end
  end
end
