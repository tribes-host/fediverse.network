defmodule Fd.Repo.Migrations.Hashtags do
  use Ecto.Migration

  def change do
    create table(:hashtags, primary_key: false) do
      add :instance_id, references(:instances, on_delete: :delete_all)
      add :ihash, :integer
      add :tag, :citext
      add :lang, :citext
      add :created_at, :"timestamp without time zone"
    end

    execute "SELECT create_hypertable('hashtags', 'created_at', chunk_time_interval => interval '1 week')"

    create index(:hashtags, [:tag])

  end
end
