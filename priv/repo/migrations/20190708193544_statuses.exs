defmodule Fd.Repo.Migrations.Statuses do
  use Ecto.Migration

  def change do
    create table(:statuses, primary_key: false) do
      add :seen_by_instance_id, references(:instances, on_delete: :delete_all)
      add :instance_id, references(:instances, on_delete: :delete_all)
      add :uhash, :integer
      add :ihash, :integer
      add :local_id, :string
      add :lang, :string
      add :created_at, :"timestamp without time zone"
      timestamps(inserted_at: false)
    end

    execute "SELECT create_hypertable('statuses', 'updated_at', chunk_time_interval => interval '1 week')"
  end

end
