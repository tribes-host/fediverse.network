defmodule Fd.Repo.Migrations.Peers do
  use Ecto.Migration

  def change do
    create table(:instance_peers, primary_key: false) do
      add :instance_id, references(:instances, on_delete: :delete_all)
      add :peered_instance_id, references(:instances, on_delete: :delete_all)
      timestamps(updated_at: false)
    end
    create unique_index(:instance_peers, [:instance_id, :peered_instance_id])
  end

end
