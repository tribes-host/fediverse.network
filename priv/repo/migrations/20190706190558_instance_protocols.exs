defmodule Fd.Repo.Migrations.InstanceProtocols do
  use Ecto.Migration

  def change do
    for table <- [:instances, :instance_checks] do
      alter table(table) do
        add(:protocols, {:array, :citext})
      end
    end
    create index(:instances, [:protocols])
  end
end
