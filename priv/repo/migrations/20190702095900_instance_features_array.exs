defmodule Fd.Repo.Migrations.InstanceFeaturesArray do
  use Ecto.Migration

  def change do
    for table <- [:instances, :instance_checks] do
      alter table(table) do
        add(:features, {:array, :text})
      end
      create index(table, [:features])
    end
  end

end
