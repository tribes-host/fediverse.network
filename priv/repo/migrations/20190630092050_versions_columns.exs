defmodule Fd.Repo.Migrations.VersionsColumns do
  use Ecto.Migration

  def change do
    for table <- [:instances, :instance_checks] do
      alter table(table) do
        add(:semver, :map)
      end
      create index(table, [:semver])
    end
  end
end
