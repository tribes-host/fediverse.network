defmodule Fd.Repo.Migrations.InstanceAs do
  use Ecto.Migration

  def change do
    alter table(:instances) do
      add(:countries, {:array, :text})
      add(:isps, {:array, :text})
    end
  end
end
