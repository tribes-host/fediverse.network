import { Controller } from "stimulus"
import Clusterize from "clusterize.js"

export default class extends Controller {

  connect() {
    console.log(this.element.getElementsByClassName("clusterize-scroll")[0])
    var clusterize = new Clusterize({
      scrollElem: this.element.getElementsByClassName("clusterize-scroll")[0],
      contentElem: this.element.getElementsByClassName("clusterize-content")[0]
    })
  }

}
