import { Controller } from "stimulus"
var d3 = require("d3")
import * as d3Color from 'd3-color'
import * as d3Interpolate from 'd3-interpolate'
import * as d3ScaleChromatic from 'd3-scale-chromatic'

// https://codepen.io/nlounds/pen/GzKwt
export default class extends Controller {
  connect() {
    const data = JSON.parse(this.element.dataset.ts);
    const scheme = this.element.dataset.scheme;
    var sortIdx = 0;
    var sortOrder = this.element.dataset.sortOrder;

    switch (this.element.dataset.sortBy) {
      case 'key':
        sortIdx = 0;
        var strcmp = new Intl.Collator(undefined, {numeric:true, sensitivity:'base'}).compare;
        data.sort(function(a,b){
          if (sortOrder === 'desc') {
            return -strcmp(a[sortIdx], b[sortIdx])
          } else {
            return strcmp(a[sortIdx], b[sortIdx]) 
          }
        })
        break;
      default:
        sortIdx = 1;
        switch (this.element.dataset.sortOrder) {
          case 'asc':
            data.sort(function(a,b){ return d3.ascending(a[sortIdx], b[sortIdx]) });
            break;
          default:
            data.sort(function(a,b){ return d3.descending(a[sortIdx], b[sortIdx]) });
        }
    }

    switch (this.element.dataset.colorScale) {
      case 'sequential':
        var z = d3.scaleSequential(d3["interpolate" + scheme])
        .domain([0.0, data.length]);
        break;
      default:
        if (data.length > 11) {
          var dl =  11;
        } else {
          var dl = data.length;
        }
        var z = d3.scaleOrdinal(d3["scheme" + scheme][dl])
        .domain([0.0, data.length]);

    }


//data.sort(function(a, b){return b[1]-a[1]});
var values = data.map(function(d) { return d[1]; });

var width = "100%",
    height = 20,
	perc_so_far = 0.0;

//var total_time = d3.sum(Object.values(data));
var total_time = d3.sum(values);
var bar_x = 0;
var chart = d3.select(this.element)
	.attr("width", "100%")
	.attr("height", height);
	bar_x = 0;
	//var chart_width = chart.style("width").replace("px", "");
var chart_width = parseInt(d3.select(this.element).style("width"));
  var z = d3.scaleSequential(d3["interpolate" + scheme])
  .domain([0.0, data.length]);

var cdl = data.length + 1;

var bar = chart.selectAll("g")
	.data(data)
	.enter().append("g");
var div = d3.select("body").append("div")
    .attr("class", "percent-horizontal-bar-tooltip")
    .style("opacity", 0);
bar.append("rect")
	//.attr("width", function(d) { return d[1]; } )
	.attr("width", function(d) { return ((d[1]/total_time)*100) + "%"; } )
	.attr("x", function(d) {
		var prev_perc = perc_so_far;
		var this_perc = 100*(d[1]/total_time);
		perc_so_far = perc_so_far + this_perc;
		return prev_perc + "%";
	})
	.attr("height", height)
	.attr("fill",  function(d) {
      cdl = cdl - 1;
      return z(cdl);
    } )
        .on("mouseover", function(d) {
            div.transition()
                .duration(200)
                .style("opacity", .9);
            div	.html(d[0] + " - " + d[2] + "% (" + d[1] + ")")
                .style("left", (d3.event.pageX) + "px")
                .style("top", (d3.event.pageY - 18) + "px");
            })
        .on("mouseout", function(d) {
            div.transition()
                .duration(500)
                .style("opacity", 0);
        })
    .append("title")
    .text(function(d) { return d[0] + " - " + d[2] + "% (" + d[1] + ")"; });

perc_so_far = 0;
bar.append("text")
	.attr("x", function(d) {
		var prev_perc = perc_so_far;
		var this_perc = 100*(d[1]/total_time);
		perc_so_far = perc_so_far + this_perc;
		return prev_perc + "%";
	})
	//.attr("y", 11)
	.attr("dy", "1.35em")
        .on("mouseover", function(d) {
            div.transition()
                .duration(200)
                .style("opacity", .9);
            div	.html(d[0] + " " + d[2] + "% (" + d[1] + ")" )
                .style("left", (d3.event.pageX) + "px")
                .style("top", (d3.event.pageY - 18) + "px");
            })
        .on("mouseout", function(d) {
            div.transition()
                .duration(500)
                .style("opacity", 0);
        })
    .text(function(d) { return d[0]; })
      .append("title")
    .text(function(d) { return d[0] + " - " + d[2] + "% (" + d[1] + ")"; });

  }

}

