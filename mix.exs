defmodule Fd.Mixfile do
  use Mix.Project

  def project do
    [
      app: :fd,
      version: "0.0.1",
      elixir: "~> 1.4",
      elixirc_paths: elixirc_paths(Mix.env),
      compilers: [:phoenix, :gettext] ++ Mix.compilers,
      start_permanent: Mix.env == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Fd.Application, []},
      extra_applications: [:logger, :runtime_tools, :earmark, :ex2ms, :parse_trans]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_),     do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.4.0"},
      {:phoenix_pubsub, "~> 1.0", override: true},
      {:phoenix_ecto, "~> 4.0"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_html, "~> 2.10"},
      {:phoenix_live_reload, "~> 1.0", only: :dev},
      {:gettext, "~> 0.11"},
      {:plug_cowboy, "~> 2.0"},
      {:cowboy, "~> 2.0"},
      {:hackney, "~> 1.12.1", override: true},
      {:httpoison, "~> 1.0", override: true},
      {:distillery, github: "bitwalker/distillery"},
      {:jason, "~> 1.0"},
      {:idna, "~> 5.1", override: true},
      {:public_suffix, "~> 0.6.0"},
      {:swoosh, "~> 0.20"},
      {:phoenix_markdown, "~> 1.0"},
      {:html_sanitize_ex, "~> 1.3.0-rc3"},
      {:timex, "~> 3.5"},
      {:earmark, "~> 1.2", runtime: true, override: true},
      {:nebulex, "~> 1.0.0-rc.3"},
      {:prometheus, "~> 4.1", override: true},
      {:prometheus_ex, "~> 3.0", override: true},
      {:prometheus_plugs, "~> 1.1.1"},
      {:prometheus_phoenix, "~> 1.0"},
      {:prometheus_ecto, "~> 1.4"},
      {:hammer, "~> 6.0"},
      {:poison, "~> 3.0"},
      {:ex_cldr, "~> 2.2", override: true},
      {:ex_cldr_numbers, "~> 2.1"},
      {:ex_cldr_lists, "~> 2.0"},
      {:ex_cldr_units, "~> 2.1"},
      {:ssl_verify_fun, "~> 1.1", override: true},
      {:certifi, "~> 2.4", override: true},
      {:ex_shards, "~> 0.2"},
      #{:prometheus_process_collector, "~> 1.0"},
      {:sentry, "~> 7.0.0"},
      {:pleroma, git: "https://git.pleroma.social/pleroma/pleroma.git", branch: "master"},
      {:cowlib, "~> 2.7.3", override: true},
      {:locus, "~> 1.6"},
      {:ex_json_schema, "~> 0.6.1"},
      {:gun, github: "ninenines/gun", tag: "1.3.0"},
      {:backoff, github: "ferd/backoff"},
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "test": ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
